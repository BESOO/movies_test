enum ErrorCodes {
  unAuth,
  wrongInput,
  forbidden,
  noConnection,
  tokenExpired,
  serverError,
  unKnownError,
  notSameDevice,
  passwordNotMatch,
  otpInvalid,
  emailWrong

}