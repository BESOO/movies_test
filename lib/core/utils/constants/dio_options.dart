import 'package:dio/dio.dart';
import 'package:movies/core/utils/constants/constants.dart';

class GetOptions {
  static Options options = Options();

  static Options getOptionsWithToken(String? token, {String language = ''}) {
    if (token != null && token.isNotEmpty) {
      options.headers = {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': "Bearer $apiKey",
        'Accept-Language': language,
      };
    } else {
      options.headers = {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Accept-Language': language,
      };
    }
    return options;
  }
}
