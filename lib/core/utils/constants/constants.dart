import 'package:flutter/widgets.dart';

class SuperHeroIcons {
  SuperHeroIcons._();

  static const _kFontExplore = 'category';
  static const _kFontHome = 'home';
  static const String? _kFontPkg = null;

  static const IconData explore =
      IconData(0xe800, fontFamily: _kFontExplore, fontPackage: _kFontPkg);
  static const IconData home =
      IconData(0xe801, fontFamily: _kFontHome, fontPackage: _kFontPkg);
}

String apiKey =
    "eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJlOWUzZGMyMjVlYjI0MTJjNzkwNDY1Yzk1ZThkZmU3OSIsInN1YiI6IjY0ZGM4YzdhMzcxMDk3MDEzOTQ2ZmEzZSIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.nqEfin50xf_JDCF9xkZBs-fvulBk0Q6ZMs6FsoPr_AU";

int accountId = 20307202;

List<String> emojis = [
  "👾",
  "🤖",
  "🎃",
  "😺",
  "😸",
  "😹",
  "😀",
  "😃",
  "😄",
  "😁",
  "😆",
  "😅",
  "😂",
  "🤣",
  "🥲",
  "🥹",
  "☺️",
  "😊",
  "😀",
  "😃",
  "😄",
  "😁",
  "😆",
  "😅",
  "😂",
  "🤣",
  "🥲",
  "🥹",
  "☺️",
  "😊",
];
