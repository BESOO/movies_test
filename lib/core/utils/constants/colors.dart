import 'package:flutter/material.dart';

class AppColors {
  static Color backgroundColor = const Color(0xFFF6F6F6);
  static Color primaryColor = const Color(0xff8078CB);

  static Color primaryBlack = const Color(0xff1d1d23);
  static Color secondaryBlack= const Color(0xff2B2B23);

  static Color secondaryFontColor = const Color(0xff4D4D5D);

  static Color categoryBackgroundColor = const Color(0xff33333D);

  static Color grayColor = const Color(0xff5B5B6F);
  static Color blackColor = const Color(0xff000000);
  static Color whiteColor = const Color(0xFFFFFFFF);

  static Color textColorGrey = const Color(0xFF7B7B7B);

}
