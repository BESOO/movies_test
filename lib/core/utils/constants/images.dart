class ImagesAssets {
  static String logo = 'assets/images/logo.svg';
  static String professorLogo = 'assets/images/professor-logo .png';
  static String privacyPolicy = 'assets/images/privacy-policy.png';
  static String library = 'assets/images/library.png';
  static String notificationIcon = 'assets/images/icons/notification.svg';
  static String searchIcon = 'assets/images/icons/search.svg';
  static String settingIcon = 'assets/images/icons/setting.svg';
  static String home1 = 'assets/images/icons/home1.svg';
  static String home2 = 'assets/images/icons/home2.svg';
  static String person  = 'assets/images/person.png'; /// ////////

  ///More screen icons
  static String videoIcon  = 'assets/images/icons/video.svg';
  static String infoIcon  = 'assets/images/icons/info.svg';
  static String trashIcon  = 'assets/images/icons/trash.svg';
  static String callIcon  = 'assets/images/icons/call.svg';
  static String locationIcon  = 'assets/images/icons/location.svg';
  static String appIcon  = 'assets/images/icons/app.svg';
  static String logoutIcon  = 'assets/images/icons/logout.svg';
  static String orangeCircle  = 'assets/images/orange-circle.png';
  static String greenCircle  = 'assets/images/green-circle.png';
  static String cyanCircle  = 'assets/images/cyan-circle.png';
  static String purpleCircle  = 'assets/images/purple-circle.png';
  static String redCircle  = 'assets/images/red-circle.png';
  static String blueCircle  = 'assets/images/blue-circle.png';


  ///Auth screens
  static String backgroundAuth  = 'assets/images/background-auth.png';
  static String backgroundPAssword  = 'assets/images/background-password.png';

  ///our centers screen
  static String ourCenters  = 'assets/images/our-centers.svg';
  static String whatsappIcons  = 'assets/images/icons/whatsapp.svg';
  static String telegramIcons  = 'assets/images/icons/telegram.svg';
  static String youtubeIcons  = 'assets/images/icons/youtube.svg';
  static String facebookIcons  = 'assets/images/icons/facebook.svg';
  static String instagramIcons  = 'assets/images/icons/instagram.png';
  static String circle  = 'assets/images/our-center-circle.png';






}


