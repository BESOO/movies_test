
/// Just if we have shared data need to be stored...
class SharedPreferencesKeys {
  static String favouriteMovies = 'favourite_movies';
  static String favouriteListIds = 'favourite_list_ids';

  static String watchingListMovies = 'watching_list_movies';
  static String watchingListIds = 'watching_list_ids';
}