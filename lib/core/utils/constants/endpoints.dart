class EndPoints {
  static String baseURL = "https://api.themoviedb.org/3";
  static String imagesBaseURL = "https://image.tmdb.org/t/p/original";

  static String createGuestSession = "$baseURL/authentication/guest_session/new";
  static String authentication = "$baseURL/authentication";

  static String genres = "$baseURL/genre/movie/list";
  static String trendingMovies = "$baseURL/trending/movie/week?language=en-US";
  static String upcomingMovies = "$baseURL/movie/upcoming";
  static String movieById(int id) => "$baseURL/movie/$id";

  static String searchMoviesByQuery(String query) => "$baseURL/search/movie?query=$query";

  static String favouriteMovies(int accountId) => "$baseURL/account/$accountId/favorite/movies";

  static String watchlistMovies(int accountId) => "$baseURL/account/$accountId/watchlist/movies";


}
