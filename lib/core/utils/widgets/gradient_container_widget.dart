import 'package:flutter/material.dart';
import 'package:movies/core/utils/extensions/extension_on_context.dart';

import '../constants/colors.dart';

class GradientContainerWidget extends StatelessWidget {
  final Widget child;

  const GradientContainerWidget({Key? key, required this.child})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        width: context.w,
        height: context.h,
        decoration: BoxDecoration(
            gradient: LinearGradient(
                colors: [AppColors.secondaryBlack, AppColors.primaryBlack],
                begin: Alignment.topLeft,
                end: Alignment.bottomRight)),
        child: child);
  }
}
