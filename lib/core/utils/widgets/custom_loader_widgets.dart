import 'package:flutter/material.dart';
import 'package:movies/core/utils/constants/colors.dart';
import 'package:movies/core/utils/extensions/extension_on_context.dart';
import 'package:shimmer/shimmer.dart';

class CustomLoaderWidgets {
  static Widget homeMoviesLoader(BuildContext context) => SizedBox(
        height: context.h * 0.3,
        child: ListView.builder(
          itemCount: 50,
          scrollDirection: Axis.horizontal,
          itemBuilder: (BuildContext context, int index) {
            return Padding(
              padding: const EdgeInsets.all(8.0),
              child: Shimmer.fromColors(
                  baseColor: AppColors.secondaryBlack,
                  highlightColor: AppColors.secondaryFontColor,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        height: context.h * 0.2,
                        width: context.w * 0.27,
                        decoration: BoxDecoration(
                            color: Colors.black.withOpacity(0.5),
                            borderRadius:
                                const BorderRadius.all(Radius.circular(12))),
                      ),
                      const SizedBox(
                        height: 4,
                      ),
                      Container(
                        height: context.h * 0.01,
                        width: context.w * 0.2,
                        decoration: BoxDecoration(
                            color: Colors.black.withOpacity(0.5),
                            borderRadius:
                                const BorderRadius.all(Radius.circular(12))),
                      ),
                      const SizedBox(
                        height: 4,
                      ),
                      Container(
                        height: context.h * 0.01,
                        width: context.w * 0.15,
                        decoration: BoxDecoration(
                            color: Colors.black.withOpacity(0.5),
                            borderRadius:
                                const BorderRadius.all(Radius.circular(12))),
                      ),
                    ],
                  )),
            );
          },
        ),
      );

  static Widget searchLoaderWidget(BuildContext context) => SizedBox(
        height: context.h * 0.3,
        child: GridView.builder(
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2, mainAxisExtent: context.h * 0.34),
          itemCount: 50,
          itemBuilder: (BuildContext context, int index) {
            return Padding(
              padding: const EdgeInsets.all(8.0),
              child: Shimmer.fromColors(
                  baseColor: AppColors.secondaryBlack,
                  highlightColor: AppColors.secondaryFontColor,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        width: double.infinity,
                        height: context.h * 0.27,
                        decoration: BoxDecoration(
                            color: Colors.black.withOpacity(0.5),
                            borderRadius:
                                const BorderRadius.all(Radius.circular(12))),
                      ),
                      const SizedBox(
                        height: 4,
                      ),
                      Container(
                        height: context.h * 0.01,
                        width: context.w * 0.2,
                        decoration: BoxDecoration(
                            color: Colors.black.withOpacity(0.5),
                            borderRadius:
                                const BorderRadius.all(Radius.circular(12))),
                      ),
                      const SizedBox(
                        height: 4,
                      ),
                      Container(
                        height: context.h * 0.01,
                        width: context.w * 0.15,
                        decoration: BoxDecoration(
                            color: Colors.black.withOpacity(0.5),
                            borderRadius:
                                const BorderRadius.all(Radius.circular(12))),
                      ),
                    ],
                  )),
            );
          },
        ),
      );

  static Widget genreLoadingWidget(BuildContext context) => SizedBox(
        height: context.h * 0.11,
        child: ListView.builder(
          itemCount: 50,
          scrollDirection: Axis.horizontal,
          itemBuilder: (BuildContext context, int index) {
            return Padding(
              padding: const EdgeInsets.all(8.0),
              child: Shimmer.fromColors(
                  baseColor: AppColors.secondaryBlack,
                  highlightColor: AppColors.secondaryFontColor,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        width: context.h * 0.075,
                        height: context.h * 0.075,
                        decoration: BoxDecoration(
                            color: Colors.black.withOpacity(0.5),
                            borderRadius:
                                const BorderRadius.all(Radius.circular(12))),
                      ),
                      const SizedBox(
                        height: 4,
                      ),
                      Container(
                        height: context.h * 0.01,
                        width: context.h * 0.065,
                        decoration: BoxDecoration(
                            color: Colors.black.withOpacity(0.5),
                            borderRadius:
                                const BorderRadius.all(Radius.circular(12))),
                      ),
                    ],
                  )),
            );
          },
        ),
      );

  static Widget savedMoviesLoaderWidget(BuildContext context) =>
      ListView.builder(
        itemCount: 50,
        scrollDirection: Axis.horizontal,
        itemBuilder: (BuildContext context, int index) {
          return Padding(
            padding: const EdgeInsets.all(8.0),
            child: Shimmer.fromColors(
                baseColor: AppColors.secondaryBlack,
                highlightColor: AppColors.secondaryFontColor,
                child: Padding(
                  padding: const EdgeInsets.only(left: 8.0, right: 8),
                  child: Container(
                    height: context.h * 0.2,
                    width: context.w,
                    decoration: BoxDecoration(
                        color: Colors.black.withOpacity(0.5),
                        borderRadius:
                            const BorderRadius.all(Radius.circular(12))),
                  ),
                )),
          );
        },
      );
}
