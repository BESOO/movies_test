import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:movies/core/utils/extensions/extension_on_context.dart';
import '../constants/colors.dart';

class InputTextField extends StatelessWidget {
  final String hintText;

  //final String labelText;
  final String errorText;
  final bool showError;
  final bool obscureText;
  final TextInputType inputType;
  final TextEditingController controller;
  final Function(String) onSubmit;
  final int maxLines;
  final bool? isFocused;
  final Color? backgroundColor;
  final bool hasLabel;
  final Widget? icon;
  final bool hasBorder;
  final Widget? iconAtFirst;
  final String labelText;

  InputTextField({
    required this.onSubmit,
    required this.hintText,
    this.errorText = '',
    this.showError = false,
    this.obscureText = false,
    this.isFocused = false,
    this.hasLabel = true,
    this.hasBorder = true,
    this.maxLines = 1,
    this.labelText = '',
    this.icon,
    this.iconAtFirst,
    this.backgroundColor = const Color(0xff141313),
    required this.controller,
    required this.inputType,
  });

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        Container(
          padding: EdgeInsets.only(
            right: 0,
          ),
          height: height * 0.05,
          decoration: BoxDecoration(
            borderRadius: const BorderRadius.all(Radius.circular(12.0)),
            border: hasBorder
                ? Border.all(
                    color: showError ? Colors.red : AppColors.primaryColor,
                    width: 1.5,
                  )
                : null,
          ),
          child: Row(
            children: [
              icon != null
                  ? Align(alignment: Alignment.centerRight, child: icon!)
                  : Container(),
              Expanded(
                child: TextField(
                  maxLines: maxLines,
                  controller: controller,
                  keyboardType: inputType,
                  obscureText: obscureText,
                  textInputAction: TextInputAction.done,
                  onSubmitted: onSubmit,
                  onChanged: (str){},
                  // textAlignVertical: TextAlignVertical.center,
                  style: TextStyle(
                    fontSize: 12.sp,
                    color: AppColors.primaryColor,
                    fontWeight: FontWeight.w500,
                  ),
                  decoration: InputDecoration(
                    suffixIcon: Icon(
                      Icons.search,
                      color: AppColors.primaryColor,
                    ),
                    border: InputBorder.none,
                    hintText: hintText,
                    hintStyle: TextStyle(
                      fontSize: 12.sp,
                      fontWeight: FontWeight.w400,
                      color: AppColors.secondaryFontColor,
                    ),
                    // contentPadding: EdgeInsets.only(bottom: height * 0.005),
                    contentPadding:
                        const EdgeInsets.symmetric(horizontal: 12, vertical: 8),
                  ),
                  cursorColor: Theme.of(context).primaryColor,
                ),
              ),
            ],
          ),
        ),
        Visibility(
          visible: showError,
          child: Padding(
            padding: const EdgeInsets.all(6),
            child: Text(
              errorText,
              style: TextStyle(
                fontSize: 10.sp,
                color: Colors.red,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        ),
      ],
    );
  }
}
