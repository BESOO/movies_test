import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

import '../constants/colors.dart';

class CustomCachedNetworkImage extends StatelessWidget {
  final String networkUrl;
  final double? width;
  final double? height;

  const CustomCachedNetworkImage({
    super.key,
    required this.networkUrl,
    this.width,
    this.height,
  });

  @override
  Widget build(BuildContext context) {
    return CachedNetworkImage(
      imageUrl: networkUrl,
      fit: BoxFit.cover,
      width: width,
      height: height,
      progressIndicatorBuilder: (context, url, downloadProgress) => Center(
        child: SpinKitPulse(
          color: AppColors.primaryColor,
        ),
      ),
      errorWidget: (context, url, error) => const Center(
        child: Text("Error!"),
      ),
    );
  }
}
