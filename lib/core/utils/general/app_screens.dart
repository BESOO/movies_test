import 'package:flutter/material.dart';
import 'package:movies/features/main_features/main_holder.dart';
import 'package:movies/features/movie_details_feature/presentation/page/movie_details_page.dart';

import '../../../features/splash_feature/presentation/page/splash_page.dart';

class ScreenGenerator {
  static Route<dynamic> onGenerate(RouteSettings value) {
    String? name = value.name;
    print("the name is $name");
    final args = value.arguments;
    switch (name) {
      case AppScreens.splashPage:
        return MaterialPageRoute(builder: (context) => const SplashPage());
      case AppScreens.mainHolderPage:
        return MaterialPageRoute(builder: (context) => const MainHolderPage());
      case AppScreens.movieDetailsPage:
        return MaterialPageRoute(
            builder: (context) => MovieDetailsPage(
                  movieModel: (args as Map)['movie'],
                ));
      default:
        return _errorRoute();
    }
  }

  static Route<dynamic> _errorRoute() {
    return MaterialPageRoute(
      builder: (_) {
        return Scaffold(
          appBar: AppBar(
            title: const Text('Error'),
          ),
          body: const Center(
            child: Text('ERROR'),
          ),
        );
      },
    );
  }
}

class AppScreens {
  static const String splashPage = "/splash_page";
  static const String mainHolderPage = "/main_holder_page";
  static const String movieDetailsPage = "/movie_details_page";
}
