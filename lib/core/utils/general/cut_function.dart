String cutS(String? link) {
  return link == null || link.isEmpty ? "" : link.replaceFirst('https', 'http');
}
