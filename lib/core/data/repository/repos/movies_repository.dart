import 'dart:convert';
import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:movies/core/data/models/genre_model.dart';
import 'package:movies/core/utils/constants/constants.dart';
import 'package:movies/core/utils/constants/dio_options.dart';
import '../../../error/exceptions.dart';
import '../../../error/failures.dart';
import '../../../utils/constants/endpoints.dart';
import '../../../utils/constants/error_codes.dart';
import 'package:injectable/injectable.dart';
import '../../models/movie_model.dart';
import '../../prefs_helpers/prefs_helper.dart';
import '../base_repository.dart';

@LazySingleton()
class MoviesRepository extends BaseRepositoryImpl {
  final PrefsHelper prefsHelper;
  final Dio dio;

  MoviesRepository({required this.prefsHelper, required this.dio})
      : super(dio: dio, prefsHelper: prefsHelper);

  Future<Either<Failure, List<MovieModel>>> getMovies(
      {required String type}) async {
    try {
      final result = await dio.get(
          type == "upcoming"
              ? EndPoints.upcomingMovies
              : EndPoints.trendingMovies,
          options: GetOptions.getOptionsWithToken(" "));
      if (json.decode(result.data) != null) {
        List<MovieModel> movies = [];
        for (var item in json.decode(result.data)['results']) {
          movies.add(MovieModel.fromJson(item));
        }
        return Right(movies);
      } else {
        if (json.decode(result.data)['message'] != null) {
          return Left(ServerFailure(ErrorCodes.unAuth,
              errorMessage: json.decode(result.data)['message']));
        } else if (json.decode(result.data) != null) {
          return Left(ServerFailure(ErrorCodes.unAuth,
              errorMessage: "UnAuthenticated!"));
        } else {
          return Left(ServerFailure(ErrorCodes.serverError,
              errorMessage: "Server error!"));
        }
      }
    } catch (e) {
      if (e is DioError) {
        if (e.response == null) {
          return Left(ServerFailure(ErrorCodes.noConnection,
              errorMessage: "No internet connection!"));
        } else {
          if (json.decode(e.response?.data) != null &&
              json.decode(e.response?.data)['message'] != null) {
            return Left(ServerFailure(ErrorCodes.serverError,
                errorMessage: json.decode(e.response?.data)['message']));
          } else {
            return Left(ServerFailure(ErrorCodes.unKnownError));
          }
        }
      } else {
        return e is ServerException
            ? Left(ServerFailure(ErrorCodes.serverError))
            : Left(ServerFailure(ErrorCodes.unKnownError));
      }
    }
  }

  Future<Either<Failure, List<MovieModel>>> moviesByQuery(
      {required String query}) async {
    try {
      final result = await dio.get(EndPoints.searchMoviesByQuery(query),
          options: GetOptions.getOptionsWithToken(" "));
      if (json.decode(result.data) != null) {
        List<MovieModel> movies = [];
        for (var item in json.decode(result.data)['results']) {
          movies.add(MovieModel.fromJson(item));
        }
        return Right(movies);
      } else {
        if (json.decode(result.data)['message'] != null) {
          return Left(ServerFailure(ErrorCodes.unAuth,
              errorMessage: json.decode(result.data)['message']));
        } else if (json.decode(result.data) != null) {
          return Left(ServerFailure(ErrorCodes.unAuth,
              errorMessage: "UnAuthenticated!"));
        } else {
          return Left(ServerFailure(ErrorCodes.serverError,
              errorMessage: "Server error!"));
        }
      }
    } catch (e) {
      if (e is DioError) {
        if (e.response == null) {
          return Left(ServerFailure(ErrorCodes.noConnection,
              errorMessage: "No internet connection!"));
        } else {
          if (json.decode(e.response?.data) != null &&
              json.decode(e.response?.data)['message'] != null) {
            return Left(ServerFailure(ErrorCodes.serverError,
                errorMessage: json.decode(e.response?.data)['message']));
          } else {
            return Left(ServerFailure(ErrorCodes.unKnownError));
          }
        }
      } else {
        return e is ServerException
            ? Left(ServerFailure(ErrorCodes.serverError))
            : Left(ServerFailure(ErrorCodes.unKnownError));
      }
    }
  }

  Future<Either<Failure, List<GenreModel>>> getGenres() async {
    try {
      final result = await dio.get(EndPoints.genres,
          options: GetOptions.getOptionsWithToken(" "));
      if (json.decode(result.data) != null) {
        List<GenreModel> genres = [];
        for (var item in json.decode(result.data)['genres']) {
          genres.add(GenreModel.fromJson(item));
        }
        return Right(genres);
      } else {
        if (json.decode(result.data)['message'] != null) {
          return Left(ServerFailure(ErrorCodes.unAuth,
              errorMessage: json.decode(result.data)['message']));
        } else if (json.decode(result.data) != null) {
          return Left(ServerFailure(ErrorCodes.unAuth,
              errorMessage: "UnAuthenticated!"));
        } else {
          return Left(ServerFailure(ErrorCodes.serverError,
              errorMessage: "Server error!"));
        }
      }
    } catch (e) {
      if (e is DioError) {
        if (e.response == null) {
          return Left(ServerFailure(ErrorCodes.noConnection,
              errorMessage: "No internet connection!"));
        } else {
          if (json.decode(e.response?.data) != null &&
              json.decode(e.response?.data)['message'] != null) {
            return Left(ServerFailure(ErrorCodes.serverError,
                errorMessage: json.decode(e.response?.data)['message']));
          } else {
            return Left(ServerFailure(ErrorCodes.unKnownError));
          }
        }
      } else {
        return e is ServerException
            ? Left(ServerFailure(ErrorCodes.serverError))
            : Left(ServerFailure(ErrorCodes.unKnownError));
      }
    }
  }

  Future<Either<Failure, List<MovieModel>>> getSavedMovies(
      {required String type}) async {
    try {
      final result = await dio.get(
          type == "favourite"
              ? EndPoints.favouriteMovies(accountId)
              : EndPoints.watchlistMovies(accountId),
          options: GetOptions.getOptionsWithToken(" "));
      if (json.decode(result.data) != null) {
        List<MovieModel> movies = [];
        for (var item in json.decode(result.data)['results']) {
          movies.add(MovieModel.fromJson(item));
        }
        return Right(movies);
      } else {
        if (json.decode(result.data)['message'] != null) {
          return Left(ServerFailure(ErrorCodes.unAuth,
              errorMessage: json.decode(result.data)['message']));
        } else if (json.decode(result.data) != null) {
          return Left(ServerFailure(ErrorCodes.unAuth,
              errorMessage: "UnAuthenticated!"));
        } else {
          return Left(ServerFailure(ErrorCodes.serverError,
              errorMessage: "Server error!"));
        }
      }
    } catch (e) {
      if (e is DioError) {
        if (e.response == null) {
          return Left(ServerFailure(ErrorCodes.noConnection,
              errorMessage: "No internet connection!"));
        } else {
          if (json.decode(e.response?.data) != null &&
              json.decode(e.response?.data)['message'] != null) {
            return Left(ServerFailure(ErrorCodes.serverError,
                errorMessage: json.decode(e.response?.data)['message']));
          } else {
            return Left(ServerFailure(ErrorCodes.unKnownError));
          }
        }
      } else {
        return e is ServerException
            ? Left(ServerFailure(ErrorCodes.serverError))
            : Left(ServerFailure(ErrorCodes.unKnownError));
      }
    }
  }
}
