import 'dart:convert';
import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:movies/core/data/prefs_helpers/prefs_helper.dart';
import 'package:movies/core/utils/constants/constants.dart';
import '../../error/failures.dart';
import '../../utils/constants/dio_options.dart';
import '../../utils/constants/endpoints.dart';
import '../../utils/constants/error_codes.dart';

abstract class BaseRepository {
  Map<String, dynamic> getBaseHeaders();

  Future<Either<Failure, bool>> logout();
}

@Injectable(as: BaseRepository)
class BaseRepositoryImpl extends BaseRepository {
  final PrefsHelper prefsHelper;
  final Dio dio;

  BaseRepositoryImpl({
    required this.prefsHelper,
    required this.dio,
  });

  @override
  Map<String, dynamic> getBaseHeaders() {
    Map<String, dynamic> headers = {};

    /// If the APIs are authenticated so we can fetch the token from shared as an example...

    headers['Authorization'] = apiKey;

    return headers;
  }

  /// Here these method we are using them in general places (different places multiple time...)..
  @override
  Future<Either<Failure, bool>> logout({bool isChangeType = false}) async {
    try {
      await dio.post("/logout",
          options: GetOptions.getOptionsWithToken(getBaseHeaders()['token']));
      return const Right(true);
    } catch (e) {
      if (e is DioError) {
        if (e.response != null &&
            json.decode(e.response!.data)['message'] == "****") {
          return Left(ServerFailure(ErrorCodes.tokenExpired));
        } else {
          return Left(ServerFailure(ErrorCodes.noConnection));
        }
      }
      print(e);
      return Left(ServerFailure(ErrorCodes.serverError));
    }
  }
}
