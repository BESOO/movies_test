// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

// ignore_for_file: unnecessary_lambdas
// ignore_for_file: lines_longer_than_80_chars
// coverage:ignore-file

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:dio/dio.dart' as _i4;
import 'package:get_it/get_it.dart' as _i1;
import 'package:injectable/injectable.dart' as _i2;
import 'package:shared_preferences/shared_preferences.dart' as _i3;

import '../../../features/main_features/home_feature/presentation/bloc/home_bloc.dart'
    as _i10;
import '../../../features/main_features/saved_movies_feature/presentation/bloc/saved_movies_bloc.dart'
    as _i8;
import '../../../features/main_features/search_feature/presentation/bloc/search_bloc.dart'
    as _i9;
import '../prefs_helpers/prefs_helper.dart' as _i5;
import '../repository/base_repository.dart' as _i6;
import '../repository/repos/movies_repository.dart' as _i7;
import 'register_module.dart' as _i11;

// ignore_for_file: unnecessary_lambdas
// ignore_for_file: lines_longer_than_80_chars
// initializes the registration of main-scope dependencies inside of GetIt
Future<_i1.GetIt> $initGetIt(
  _i1.GetIt getIt, {
  String? environment,
  _i2.EnvironmentFilter? environmentFilter,
}) async {
  final gh = _i2.GetItHelper(
    getIt,
    environment,
    environmentFilter,
  );
  final registerModule = _$RegisterModule();
  await gh.lazySingletonAsync<_i3.SharedPreferences>(
    () => registerModule.prefs,
    preResolve: true,
  );
  gh.lazySingleton<_i4.Dio>(
      () => registerModule.dio(gh<_i3.SharedPreferences>()));
  gh.lazySingleton<_i5.PrefsHelper>(
      () => _i5.PrefsHelper(gh<_i3.SharedPreferences>()));
  gh.factory<_i6.BaseRepository>(() => _i6.BaseRepositoryImpl(
        prefsHelper: gh<_i5.PrefsHelper>(),
        dio: gh<_i4.Dio>(),
      ));
  gh.lazySingleton<_i7.MoviesRepository>(() => _i7.MoviesRepository(
        prefsHelper: gh<_i5.PrefsHelper>(),
        dio: gh<_i4.Dio>(),
      ));
  gh.factory<_i8.SavedMoviesBloc>(
      () => _i8.SavedMoviesBloc(gh<_i7.MoviesRepository>()));
  gh.factory<_i9.SearchBloc>(() => _i9.SearchBloc(gh<_i7.MoviesRepository>()));
  gh.factory<_i10.HomeBloc>(() => _i10.HomeBloc(gh<_i7.MoviesRepository>()));
  return getIt;
}

class _$RegisterModule extends _i11.RegisterModule {}
