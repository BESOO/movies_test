import 'dart:convert';

class MovieModel {
  final int id;
  final bool adult;
  final String? backdropPath;
  final String originalLanguage;
  final String originalTitle;
  final String overview;
  final String popularity;
  final String? posterPath;
  final String releaseDate;
  final String title;
  final num voteCount;
  final num voteAverage;

  MovieModel(
      {required this.id,
      required this.adult,
      required this.backdropPath,
      required this.originalLanguage,
      required this.originalTitle,
      required this.overview,
      required this.popularity,
      required this.posterPath,
      required this.releaseDate,
      required this.title,
      required this.voteCount,
      required this.voteAverage});

  factory MovieModel.fromJson(Map<String, dynamic> json) {
    return MovieModel(
        id: json['id'],
        adult: json['adult'],
        backdropPath: json['backdrop_path'],
        originalLanguage: json['original_language'],
        originalTitle: json['original_title'],
        overview: json['overview'],
        popularity: json['popularity'].toString(),
        posterPath: json['poster_path'],
        releaseDate: json['release_date'],
        title: json['title'],
        voteCount: json['vote_count'],
        voteAverage: json['vote_average']);
  }

  static Map<String, dynamic> toMap(MovieModel movie) {
    return {
      'id': movie.id,
      'adult': movie.adult,
      'backdrop_path': movie.backdropPath,
      'original_language': movie.originalLanguage,
      'original_title': movie.originalTitle,
      'overview': movie.overview,
      'popularity': movie.popularity,
      'poster_path': movie.posterPath,
      'release_date': movie.releaseDate,
      'title': movie.title,
      'vote_count': movie.voteCount,
      'vote_average': movie.voteAverage,
    };
  }

  static String encode(List<MovieModel> videos) => json.encode(
        videos
            .map<Map<String, dynamic>>((video) => MovieModel.toMap(video))
            .toList(),
      );

  static List<MovieModel> decode(String videos) =>
      (json.decode(videos) as List<dynamic>)
          .map<MovieModel>((item) => MovieModel.fromJson(item))
          .toList();
}
