import 'dart:convert';
import 'package:movies/core/data/models/movie_model.dart';
import 'package:movies/core/utils/constants/shared_keys.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:injectable/injectable.dart';

/// Just if we have shared data need to be stored...
@LazySingleton()
class PrefsHelper {
  final SharedPreferences prefs;

  PrefsHelper(this.prefs);

  Future<List<MovieModel>> getFavouriteMovies() async {
    final String? videosStr =
        prefs.getString(SharedPreferencesKeys.favouriteMovies);
    if (videosStr == null) {
      return [];
    } else {
      final List<MovieModel> movies = MovieModel.decode(videosStr);
      return movies;
    }
  }

  Future<List<MovieModel>> getWatchingListMovies() async {
    final String? videosStr =
        prefs.getString(SharedPreferencesKeys.watchingListMovies);
    if (videosStr == null) {
      return [];
    } else {
      final List<MovieModel> movies = MovieModel.decode(videosStr);
      return movies;
    }
  }

  List<int> getStoredFavouriteListIds() {
    List<String> mList =
        (prefs.getStringList(SharedPreferencesKeys.favouriteListIds) ?? []);
    return mList.map((i) => int.parse(i)).toList();
  }

  List<int> getStoredWatchingListIds() {
    List<String> mList =
        (prefs.getStringList(SharedPreferencesKeys.watchingListIds) ?? []);
    return mList.map((i) => int.parse(i)).toList();
  }

  Future<void> addMovieToFavouriteList(MovieModel movieModel) async {
    List<MovieModel> movies = await getFavouriteMovies();
    movies.add(movieModel);
    final String encodedData = MovieModel.encode(movies);
    await prefs.setString(SharedPreferencesKeys.favouriteMovies, encodedData);
  }

  void addMovieIdToFavouriteList(int videoId) {
    List<int> videosIds = getStoredFavouriteListIds();
    if (!videosIds.contains(videoId)) {
      videosIds.add(videoId);
    }
    List<String> stringsList = videosIds.map((i) => i.toString()).toList();
    prefs.setStringList(SharedPreferencesKeys.favouriteListIds, stringsList);
  }

  Future<void> removeMovieToFavouriteList(MovieModel movieModel) async {
    List<MovieModel> movies = await getFavouriteMovies();
    movies.removeWhere((element) => element.id == movieModel.id);
    final String encodedData = MovieModel.encode(movies);
    await prefs.setString(SharedPreferencesKeys.favouriteMovies, encodedData);
  }

  void removeMovieIdToFavouriteList(int videoId) {
    List<int> videosIds = getStoredFavouriteListIds();
    if (videosIds.contains(videoId)) {
      videosIds.remove(videoId);
    }
    List<String> stringsList = videosIds.map((i) => i.toString()).toList();
    prefs.setStringList(SharedPreferencesKeys.favouriteListIds, stringsList);
  }

  Future<void> addMovieToWatchingList(MovieModel movieModel) async {
    List<MovieModel> movies = await getWatchingListMovies();
    movies.add(movieModel);
    final String encodedData = MovieModel.encode(movies);
    await prefs.setString(
        SharedPreferencesKeys.watchingListMovies, encodedData);
  }

  void addMovieIdToWatchingList(int videoId) {
    List<int> videosIds = getStoredWatchingListIds();
    if (!videosIds.contains(videoId)) {
      videosIds.add(videoId);
    }
    List<String> stringsList = videosIds.map((i) => i.toString()).toList();
    prefs.setStringList(SharedPreferencesKeys.watchingListIds, stringsList);
  }

  Future<void> removeMovieToWatchingList(MovieModel movieModel) async {
    List<MovieModel> movies = await getWatchingListMovies();
    movies.removeWhere((element) => element.id == movieModel.id);
    final String encodedData = MovieModel.encode(movies);
    await prefs.setString(SharedPreferencesKeys.watchingListIds, encodedData);
  }

  void removeMovieIdToWatchingList(int videoId) {
    List<int> videosIds = getStoredWatchingListIds();
    if (videosIds.contains(videoId)) {
      videosIds.remove(videoId);
    }
    List<String> stringsList = videosIds.map((i) => i.toString()).toList();
    prefs.setStringList(SharedPreferencesKeys.watchingListIds, stringsList);
  }
}
