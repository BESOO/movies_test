import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:movies/core/data/models/movie_model.dart';
import 'package:movies/core/utils/constants/colors.dart';
import 'package:movies/core/utils/constants/endpoints.dart';
import 'package:movies/core/utils/extensions/extension_on_context.dart';
import 'package:movies/core/utils/widgets/gradient_container_widget.dart';

class MovieDetailsPage extends StatefulWidget {
  final MovieModel movieModel;

  const MovieDetailsPage({Key? key, required this.movieModel})
      : super(key: key);

  @override
  State<MovieDetailsPage> createState() => _MovieDetailsPageState();
}

class _MovieDetailsPageState extends State<MovieDetailsPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: InkWell(
          onTap: () {
            Navigator.pop(context);
          },
          child: const SizedBox(
            width: 40,
            height: 40,
            child: Center(
              child: Icon(
                Icons.arrow_back_ios,
                color: Colors.white,
                size: 18,
              ),
            ),
          ),
        ),
        title: Text(
          "Movie Details",
          style: TextStyle(color: Colors.white, fontSize: 12.sp),
        ),
      ),
      body: GradientContainerWidget(
        child: SingleChildScrollView(
            child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: context.h * 0.02,
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Expanded(
                    child: Padding(
                  padding: const EdgeInsets.only(left: 16.0, right: 16.0),
                  child: Container(
                    height: context.h * 0.35,
                    decoration: BoxDecoration(
                        color: widget.movieModel.posterPath == null
                            ? Colors.grey.withOpacity(0.1)
                            : Colors.transparent,
                        borderRadius:
                            const BorderRadius.all(Radius.circular(20))),
                    child: ClipRRect(
                      borderRadius: const BorderRadius.all(Radius.circular(20)),
                      child: widget.movieModel.posterPath == null
                          ? Icon(
                              Icons.image_outlined,
                              color: Colors.grey,
                              size: context.w * 0.15,
                            )
                          : CachedNetworkImage(
                              imageUrl: EndPoints.imagesBaseURL +
                                  widget.movieModel.posterPath!,
                              fit: BoxFit.cover,
                            ),
                    ),
                  ),
                )),
                SizedBox(
                  width: context.w * 0.05,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      width: context.w * 0.2,
                      height: context.h * 0.1,
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.white),
                          borderRadius:
                              const BorderRadius.all(Radius.circular(20))),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Icon(
                              Icons.slow_motion_video_outlined,
                              color: Colors.white,
                              size: 20.sp,
                            ),
                            Text(
                              "Genre",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 11.sp),
                            ),

                            /// Here We can request the genre api and matching between the genre ids and api genre ids and put it here...
                            Text(
                              "Action",
                              style: TextStyle(
                                  color: AppColors.secondaryFontColor,
                                  fontSize: 11.sp,
                                  fontWeight: FontWeight.bold),
                            )
                          ],
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 16,
                    ),
                    Container(
                      width: context.w * 0.2,
                      height: context.h * 0.1,
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.white),
                          borderRadius:
                              const BorderRadius.all(Radius.circular(20))),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Icon(
                              Icons.access_time_outlined,
                              color: Colors.white,
                              size: 20.sp,
                            ),
                            Text(
                              "Duration",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 11.sp),
                            ),

                            /// Here We can request the genre api and matching between the genre ids and api genre ids and put it here...
                            Text(
                              "131 mins",
                              style: TextStyle(
                                  color: AppColors.secondaryFontColor,
                                  fontSize: 11.sp,
                                  fontWeight: FontWeight.bold),
                            )
                          ],
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 16,
                    ),
                    Container(
                      width: context.w * 0.2,
                      height: context.h * 0.1,
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.white),
                          borderRadius:
                              const BorderRadius.all(Radius.circular(20))),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Icon(
                              Icons.star_rate,
                              color: Colors.white,
                              size: 20.sp,
                            ),
                            Text(
                              "Rating",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 11.sp),
                            ),

                            /// Here We can request the genre api and matching between the genre ids and api genre ids and put it here...
                            Text(
                              widget.movieModel.voteAverage.toString(),
                              style: TextStyle(
                                  color: AppColors.secondaryFontColor,
                                  fontSize: 11.sp,
                                  fontWeight: FontWeight.bold),
                            )
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  width: context.w * 0.1,
                ),
              ],
            ),
            SizedBox(
              height: context.h * 0.03,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16.0),
              child: Text(
                widget.movieModel.originalTitle,
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 18.sp),
              ),
            ),
            SizedBox(
              height: context.h * 0.02,
            ),
            getMovieDetailsButtons(),
            SizedBox(
              height: context.h * 0.02,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16.0),
              child: Text(
                "Plot Summary",
                style: TextStyle(
                    color: AppColors.secondaryFontColor,
                    fontWeight: FontWeight.bold,
                    fontSize: 13.sp),
              ),
            ),
            SizedBox(
              height: context.h * 0.01,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16.0),
              child: Text(
                widget.movieModel.overview,
                style: TextStyle(color: Colors.white, fontSize: 11.sp),
              ),
            ),
          ],
        )),
      ),
    );
  }

  Widget getMovieDetailsButtons() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16.0),
      child: Row(
        children: [
          Container(
            decoration: BoxDecoration(
                border: Border.all(color: AppColors.primaryColor),
                borderRadius: BorderRadius.all(Radius.circular(12))),
            child: Padding(
              padding: const EdgeInsets.all(12.0),
              child: Row(
                children: [
                  Icon(
                    Icons.play_circle,
                    color: Colors.white,
                    size: 22.sp,
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Text(
                    "Watch Tailer",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 11.sp,
                        fontWeight: FontWeight.bold),
                  )
                ],
              ),
            ),
          ),
          SizedBox(
            width: 10,
          ),
          Expanded(
            child: Container(
              decoration: BoxDecoration(
                  border: Border.all(color: AppColors.primaryColor),
                  borderRadius: BorderRadius.all(Radius.circular(12))),
              child: Padding(
                padding: const EdgeInsets.all(12.0),
                child: Row(
                  children: [
                    Icon(
                      Icons.library_books_outlined,
                      color: Colors.white,
                      size: 22.sp,
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Expanded(
                      child: Text(
                        "Add To My Watching List",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 11.sp,
                            fontWeight: FontWeight.bold),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
