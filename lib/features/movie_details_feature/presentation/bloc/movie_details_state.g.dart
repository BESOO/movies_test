// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'movie_details_state.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$MovieDetailsState extends MovieDetailsState {
  factory _$MovieDetailsState(
          [void Function(MovieDetailsStateBuilder)? updates]) =>
      (new MovieDetailsStateBuilder()..update(updates))._build();

  _$MovieDetailsState._() : super._();

  @override
  MovieDetailsState rebuild(void Function(MovieDetailsStateBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  MovieDetailsStateBuilder toBuilder() =>
      new MovieDetailsStateBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is MovieDetailsState;
  }

  @override
  int get hashCode {
    return 788158201;
  }

  @override
  String toString() {
    return newBuiltValueToStringHelper(r'MovieDetailsState').toString();
  }
}

class MovieDetailsStateBuilder
    implements Builder<MovieDetailsState, MovieDetailsStateBuilder> {
  _$MovieDetailsState? _$v;

  MovieDetailsStateBuilder();

  @override
  void replace(MovieDetailsState other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$MovieDetailsState;
  }

  @override
  void update(void Function(MovieDetailsStateBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  MovieDetailsState build() => _build();

  _$MovieDetailsState _build() {
    final _$result = _$v ?? new _$MovieDetailsState._();
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
