import 'package:built_value/built_value.dart';
import 'package:movies/core/data/models/movie_model.dart';
import 'package:movies/core/error/failures.dart';

part 'movie_details_state.g.dart';

abstract class MovieDetailsState
    implements Built<MovieDetailsState, MovieDetailsStateBuilder> {
  MovieDetailsState._();

  factory MovieDetailsState([Function(MovieDetailsStateBuilder b) updates]) =
      _$MovieDetailsState;

  factory MovieDetailsState.initial() {
    return MovieDetailsState((b) => b);
  }
}
