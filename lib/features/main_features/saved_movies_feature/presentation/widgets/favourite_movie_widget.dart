import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:movies/core/data/models/movie_model.dart';
import 'package:movies/core/utils/constants/colors.dart';
import 'package:movies/core/utils/extensions/extension_on_context.dart';

import '../../../../../core/utils/constants/endpoints.dart';

class FavouriteMovieWidget extends StatelessWidget {
  final MovieModel movie;

  const FavouriteMovieWidget({Key? key, required this.movie}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
      child: Container(
        width: context.w,
        height: context.h * 0.13,
        decoration: BoxDecoration(
            border: Border.all(color: AppColors.primaryColor, width: 1.5),
            borderRadius: BorderRadius.all(Radius.circular(20))),
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0),
          child: Row(
            children: [
              Container(
                height: context.h * 0.1,
                width: context.w * 0.15,
                decoration: BoxDecoration(
                    color: movie.posterPath == null
                        ? Colors.grey.withOpacity(0.1)
                        : Colors.transparent,
                    borderRadius: const BorderRadius.all(Radius.circular(20))),
                child: ClipRRect(
                    borderRadius: const BorderRadius.all(Radius.circular(20)),
                    child: movie.posterPath == null
                        ? Icon(
                            Icons.image_outlined,
                            color: Colors.grey,
                            size: context.w * 0.15,
                          )
                        : CachedNetworkImage(
                            imageUrl:
                                EndPoints.imagesBaseURL + movie.posterPath!,
                            fit: BoxFit.cover,
                          )),
              ),
              SizedBox(
                width: 16,
              ),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      movie.originalTitle,
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 12.sp,
                          color: AppColors.primaryColor),
                    ),
                    SizedBox(
                      height: 6,
                    ),
                    Text(
                      movie.overview,
                      maxLines: 3,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        fontSize: 10.sp,
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                width: 16,
              ),
              Icon(
                Icons.delete,
                size: 22.sp,
                color: Colors.white,
              )
            ],
          ),
        ),
      ),
    );
  }
}
