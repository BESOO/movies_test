// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'saved_movies_state.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$SavedMoviesState extends SavedMoviesState {
  @override
  final bool isLoadingFavouriteMovies;
  @override
  final bool errorLoadingFavouriteMovies;
  @override
  final bool favouriteMoviesLoaded;
  @override
  final List<MovieModel> favouriteMovies;
  @override
  final Failure? failure;
  @override
  final bool isLoadingWatchingListMovies;
  @override
  final bool errorLoadingWatchingListMovies;
  @override
  final bool watchingListMoviesLoaded;
  @override
  final List<MovieModel> watchingListMovies;

  factory _$SavedMoviesState(
          [void Function(SavedMoviesStateBuilder)? updates]) =>
      (new SavedMoviesStateBuilder()..update(updates))._build();

  _$SavedMoviesState._(
      {required this.isLoadingFavouriteMovies,
      required this.errorLoadingFavouriteMovies,
      required this.favouriteMoviesLoaded,
      required this.favouriteMovies,
      this.failure,
      required this.isLoadingWatchingListMovies,
      required this.errorLoadingWatchingListMovies,
      required this.watchingListMoviesLoaded,
      required this.watchingListMovies})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(isLoadingFavouriteMovies,
        r'SavedMoviesState', 'isLoadingFavouriteMovies');
    BuiltValueNullFieldError.checkNotNull(errorLoadingFavouriteMovies,
        r'SavedMoviesState', 'errorLoadingFavouriteMovies');
    BuiltValueNullFieldError.checkNotNull(
        favouriteMoviesLoaded, r'SavedMoviesState', 'favouriteMoviesLoaded');
    BuiltValueNullFieldError.checkNotNull(
        favouriteMovies, r'SavedMoviesState', 'favouriteMovies');
    BuiltValueNullFieldError.checkNotNull(isLoadingWatchingListMovies,
        r'SavedMoviesState', 'isLoadingWatchingListMovies');
    BuiltValueNullFieldError.checkNotNull(errorLoadingWatchingListMovies,
        r'SavedMoviesState', 'errorLoadingWatchingListMovies');
    BuiltValueNullFieldError.checkNotNull(watchingListMoviesLoaded,
        r'SavedMoviesState', 'watchingListMoviesLoaded');
    BuiltValueNullFieldError.checkNotNull(
        watchingListMovies, r'SavedMoviesState', 'watchingListMovies');
  }

  @override
  SavedMoviesState rebuild(void Function(SavedMoviesStateBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  SavedMoviesStateBuilder toBuilder() =>
      new SavedMoviesStateBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is SavedMoviesState &&
        isLoadingFavouriteMovies == other.isLoadingFavouriteMovies &&
        errorLoadingFavouriteMovies == other.errorLoadingFavouriteMovies &&
        favouriteMoviesLoaded == other.favouriteMoviesLoaded &&
        favouriteMovies == other.favouriteMovies &&
        failure == other.failure &&
        isLoadingWatchingListMovies == other.isLoadingWatchingListMovies &&
        errorLoadingWatchingListMovies ==
            other.errorLoadingWatchingListMovies &&
        watchingListMoviesLoaded == other.watchingListMoviesLoaded &&
        watchingListMovies == other.watchingListMovies;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, isLoadingFavouriteMovies.hashCode);
    _$hash = $jc(_$hash, errorLoadingFavouriteMovies.hashCode);
    _$hash = $jc(_$hash, favouriteMoviesLoaded.hashCode);
    _$hash = $jc(_$hash, favouriteMovies.hashCode);
    _$hash = $jc(_$hash, failure.hashCode);
    _$hash = $jc(_$hash, isLoadingWatchingListMovies.hashCode);
    _$hash = $jc(_$hash, errorLoadingWatchingListMovies.hashCode);
    _$hash = $jc(_$hash, watchingListMoviesLoaded.hashCode);
    _$hash = $jc(_$hash, watchingListMovies.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'SavedMoviesState')
          ..add('isLoadingFavouriteMovies', isLoadingFavouriteMovies)
          ..add('errorLoadingFavouriteMovies', errorLoadingFavouriteMovies)
          ..add('favouriteMoviesLoaded', favouriteMoviesLoaded)
          ..add('favouriteMovies', favouriteMovies)
          ..add('failure', failure)
          ..add('isLoadingWatchingListMovies', isLoadingWatchingListMovies)
          ..add(
              'errorLoadingWatchingListMovies', errorLoadingWatchingListMovies)
          ..add('watchingListMoviesLoaded', watchingListMoviesLoaded)
          ..add('watchingListMovies', watchingListMovies))
        .toString();
  }
}

class SavedMoviesStateBuilder
    implements Builder<SavedMoviesState, SavedMoviesStateBuilder> {
  _$SavedMoviesState? _$v;

  bool? _isLoadingFavouriteMovies;
  bool? get isLoadingFavouriteMovies => _$this._isLoadingFavouriteMovies;
  set isLoadingFavouriteMovies(bool? isLoadingFavouriteMovies) =>
      _$this._isLoadingFavouriteMovies = isLoadingFavouriteMovies;

  bool? _errorLoadingFavouriteMovies;
  bool? get errorLoadingFavouriteMovies => _$this._errorLoadingFavouriteMovies;
  set errorLoadingFavouriteMovies(bool? errorLoadingFavouriteMovies) =>
      _$this._errorLoadingFavouriteMovies = errorLoadingFavouriteMovies;

  bool? _favouriteMoviesLoaded;
  bool? get favouriteMoviesLoaded => _$this._favouriteMoviesLoaded;
  set favouriteMoviesLoaded(bool? favouriteMoviesLoaded) =>
      _$this._favouriteMoviesLoaded = favouriteMoviesLoaded;

  List<MovieModel>? _favouriteMovies;
  List<MovieModel>? get favouriteMovies => _$this._favouriteMovies;
  set favouriteMovies(List<MovieModel>? favouriteMovies) =>
      _$this._favouriteMovies = favouriteMovies;

  Failure? _failure;
  Failure? get failure => _$this._failure;
  set failure(Failure? failure) => _$this._failure = failure;

  bool? _isLoadingWatchingListMovies;
  bool? get isLoadingWatchingListMovies => _$this._isLoadingWatchingListMovies;
  set isLoadingWatchingListMovies(bool? isLoadingWatchingListMovies) =>
      _$this._isLoadingWatchingListMovies = isLoadingWatchingListMovies;

  bool? _errorLoadingWatchingListMovies;
  bool? get errorLoadingWatchingListMovies =>
      _$this._errorLoadingWatchingListMovies;
  set errorLoadingWatchingListMovies(bool? errorLoadingWatchingListMovies) =>
      _$this._errorLoadingWatchingListMovies = errorLoadingWatchingListMovies;

  bool? _watchingListMoviesLoaded;
  bool? get watchingListMoviesLoaded => _$this._watchingListMoviesLoaded;
  set watchingListMoviesLoaded(bool? watchingListMoviesLoaded) =>
      _$this._watchingListMoviesLoaded = watchingListMoviesLoaded;

  List<MovieModel>? _watchingListMovies;
  List<MovieModel>? get watchingListMovies => _$this._watchingListMovies;
  set watchingListMovies(List<MovieModel>? watchingListMovies) =>
      _$this._watchingListMovies = watchingListMovies;

  SavedMoviesStateBuilder();

  SavedMoviesStateBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _isLoadingFavouriteMovies = $v.isLoadingFavouriteMovies;
      _errorLoadingFavouriteMovies = $v.errorLoadingFavouriteMovies;
      _favouriteMoviesLoaded = $v.favouriteMoviesLoaded;
      _favouriteMovies = $v.favouriteMovies;
      _failure = $v.failure;
      _isLoadingWatchingListMovies = $v.isLoadingWatchingListMovies;
      _errorLoadingWatchingListMovies = $v.errorLoadingWatchingListMovies;
      _watchingListMoviesLoaded = $v.watchingListMoviesLoaded;
      _watchingListMovies = $v.watchingListMovies;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(SavedMoviesState other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$SavedMoviesState;
  }

  @override
  void update(void Function(SavedMoviesStateBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  SavedMoviesState build() => _build();

  _$SavedMoviesState _build() {
    final _$result = _$v ??
        new _$SavedMoviesState._(
            isLoadingFavouriteMovies: BuiltValueNullFieldError.checkNotNull(
                isLoadingFavouriteMovies, r'SavedMoviesState', 'isLoadingFavouriteMovies'),
            errorLoadingFavouriteMovies: BuiltValueNullFieldError.checkNotNull(
                errorLoadingFavouriteMovies, r'SavedMoviesState', 'errorLoadingFavouriteMovies'),
            favouriteMoviesLoaded: BuiltValueNullFieldError.checkNotNull(
                favouriteMoviesLoaded, r'SavedMoviesState', 'favouriteMoviesLoaded'),
            favouriteMovies: BuiltValueNullFieldError.checkNotNull(
                favouriteMovies, r'SavedMoviesState', 'favouriteMovies'),
            failure: failure,
            isLoadingWatchingListMovies: BuiltValueNullFieldError.checkNotNull(
                isLoadingWatchingListMovies, r'SavedMoviesState', 'isLoadingWatchingListMovies'),
            errorLoadingWatchingListMovies: BuiltValueNullFieldError.checkNotNull(
                errorLoadingWatchingListMovies, r'SavedMoviesState', 'errorLoadingWatchingListMovies'),
            watchingListMoviesLoaded: BuiltValueNullFieldError.checkNotNull(
                watchingListMoviesLoaded, r'SavedMoviesState', 'watchingListMoviesLoaded'),
            watchingListMovies: BuiltValueNullFieldError.checkNotNull(watchingListMovies, r'SavedMoviesState', 'watchingListMovies'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
