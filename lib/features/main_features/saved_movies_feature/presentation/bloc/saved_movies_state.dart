import 'package:built_value/built_value.dart';
import 'package:movies/core/data/models/movie_model.dart';
import 'package:movies/core/error/failures.dart';

part 'saved_movies_state.g.dart';

abstract class SavedMoviesState
    implements Built<SavedMoviesState, SavedMoviesStateBuilder> {
  SavedMoviesState._();

  factory SavedMoviesState([Function(SavedMoviesStateBuilder b) updates]) =
      _$SavedMoviesState;

  bool get isLoadingFavouriteMovies;
  bool get errorLoadingFavouriteMovies;
  bool get favouriteMoviesLoaded;
  List<MovieModel> get favouriteMovies;

  Failure? get failure;

  bool get isLoadingWatchingListMovies;
  bool get errorLoadingWatchingListMovies;
  bool get watchingListMoviesLoaded;
  List<MovieModel> get watchingListMovies;

  factory SavedMoviesState.initial() {
    return SavedMoviesState((b) => b
      ..isLoadingWatchingListMovies = false
      ..errorLoadingWatchingListMovies = false
      ..watchingListMoviesLoaded = false
      ..isLoadingFavouriteMovies = false
      ..errorLoadingFavouriteMovies = false
      ..favouriteMoviesLoaded = false
      ..favouriteMovies = []
      ..watchingListMovies = []
    );
  }
}
