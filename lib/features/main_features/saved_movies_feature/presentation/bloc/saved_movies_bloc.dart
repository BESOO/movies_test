import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';
import 'package:movies/core/data/repository/repos/movies_repository.dart';
import 'package:movies/features/main_features/saved_movies_feature/presentation/bloc/saved_movies_event.dart';
import 'package:movies/features/main_features/saved_movies_feature/presentation/bloc/saved_movies_state.dart';

@Injectable()
class SavedMoviesBloc extends Bloc<SavedMoviesEvent, SavedMoviesState> {
  final MoviesRepository moviesRepository;

  SavedMoviesBloc(this.moviesRepository) : super(SavedMoviesState.initial());

  @override
  Stream<SavedMoviesState> mapEventToState(SavedMoviesEvent event) async* {
    if (event is GetFavouriteMovies) {
      yield* mapToGetFavouriteMovies();
    } else if (event is GetWatchingListMovies) {
      yield* mapToGetWatchingListMovies();
    }
  }

  Stream<SavedMoviesState> mapToGetFavouriteMovies() async* {
    yield state.rebuild((p0) => p0
      ..isLoadingFavouriteMovies = true
      ..errorLoadingFavouriteMovies = false
      ..favouriteMoviesLoaded = false);
    final result = await moviesRepository.prefsHelper.getFavouriteMovies();
    yield state.rebuild((p0) => p0
      ..favouriteMovies = result
      ..isLoadingFavouriteMovies = false
      ..errorLoadingFavouriteMovies = false
      ..favouriteMoviesLoaded = true);
  }

  Stream<SavedMoviesState> mapToGetWatchingListMovies() async* {
    yield state.rebuild((p0) => p0
      ..isLoadingWatchingListMovies = true
      ..errorLoadingWatchingListMovies = false
      ..watchingListMoviesLoaded = false);
    final result = await moviesRepository.prefsHelper.getWatchingListMovies();
    yield state.rebuild((p0) => p0
      ..watchingListMovies = result
      ..isLoadingWatchingListMovies = false
      ..errorLoadingWatchingListMovies = false
      ..watchingListMoviesLoaded = true);
  }
}
