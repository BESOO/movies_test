abstract class SavedMoviesEvent {}

class GetFavouriteMovies extends SavedMoviesEvent {}

class GetWatchingListMovies extends SavedMoviesEvent {}
