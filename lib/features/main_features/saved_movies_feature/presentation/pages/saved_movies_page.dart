import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:movies/core/utils/constants/colors.dart';
import 'package:movies/features/main_features/saved_movies_feature/presentation/bloc/saved_movies_bloc.dart';
import 'package:movies/features/main_features/saved_movies_feature/presentation/pages/sub_pages/favourite_list_page.dart';
import 'package:movies/features/main_features/saved_movies_feature/presentation/pages/sub_pages/watching_list_page.dart';
import '../../../../../core/utils/widgets/gradient_container_widget.dart';

class SavedMoviesPage extends StatefulWidget {
  final SavedMoviesBloc bloc;

  const SavedMoviesPage({Key? key, required this.bloc}) : super(key: key);

  @override
  State<SavedMoviesPage> createState() => _SavedMoviesPageState();
}

class _SavedMoviesPageState extends State<SavedMoviesPage>
    with SingleTickerProviderStateMixin {
  late TabController _tabController;

  @override
  void initState() {
    _tabController = TabController(length: 2, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(0),
          child: AppBar(),
        ),
        body: GradientContainerWidget(
          child: Column(
            children: [
              TabBar(
                  indicatorColor: AppColors.primaryColor,
                  indicatorWeight: 5,
                  controller: _tabController,
                  tabs: <Tab>[
                    Tab(
                        icon: Text(
                      "Favourite Movies",
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontSize: 12.sp),
                    )),
                    Tab(
                        icon: Text(
                      "Watching List",
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontSize: 12.sp),
                    )),
                  ]),
              Expanded(
                child: TabBarView(
                  controller: _tabController,
                  children: [
                    FavouriteListPage(bloc: widget.bloc),
                    WatchingListPage(bloc: widget.bloc)
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
