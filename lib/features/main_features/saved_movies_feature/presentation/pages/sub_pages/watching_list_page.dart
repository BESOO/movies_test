import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:movies/core/utils/widgets/custom_loader_widgets.dart';
import 'package:movies/features/main_features/saved_movies_feature/presentation/bloc/saved_movies_bloc.dart';
import 'package:movies/features/main_features/saved_movies_feature/presentation/bloc/saved_movies_event.dart';
import 'package:movies/features/main_features/saved_movies_feature/presentation/bloc/saved_movies_state.dart';
import 'package:movies/features/main_features/saved_movies_feature/presentation/widgets/favourite_movie_widget.dart';

class WatchingListPage extends StatefulWidget {
  final SavedMoviesBloc bloc;

  const WatchingListPage({Key? key, required this.bloc}) : super(key: key);

  @override
  State<WatchingListPage> createState() => _WatchListPageState();
}

class _WatchListPageState extends State<WatchingListPage> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder(
      bloc: widget.bloc,
      builder: (BuildContext context, SavedMoviesState state) {
        if (state.isLoadingWatchingListMovies) {
          return CustomLoaderWidgets.savedMoviesLoaderWidget(context);
        } else if (state.watchingListMovies.isEmpty) {
          return GestureDetector(
            onTap: () {
              widget.bloc.add(GetWatchingListMovies());
            },
            child: Center(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(
                    Icons.refresh,
                    color: Colors.white,
                    size: 17.sp,
                  ),
                  const SizedBox(width: 8,),
                  Text(
                    "No items found",
                    style: TextStyle(color: Colors.white, fontSize: 12.sp),
                  ),
                ],
              ),
            ),
          );
        } else {
          return Padding(
            padding: const EdgeInsets.only(top: 16.0),
            child: RefreshIndicator(
              onRefresh: () {
                widget.bloc.add(GetWatchingListMovies());
                return Future.value();
              },
              child: ListView.builder(
                  itemCount: state.watchingListMovies.length,
                  itemBuilder: (BuildContext context, int index) {
                    return FavouriteMovieWidget(
                        movie: state.watchingListMovies[index]);
                  }),
            ),
          );
        }
      },
    );
  }
}
