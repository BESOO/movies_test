import 'package:animations/animations.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_nav_bar/google_nav_bar.dart';
import 'package:movies/core/utils/constants/colors.dart';
import 'package:movies/core/utils/constants/constants.dart';
import 'package:movies/core/utils/extensions/extension_on_context.dart';
import 'package:movies/features/main_features/home_feature/presentation/bloc/home_bloc.dart';
import 'package:movies/features/main_features/home_feature/presentation/bloc/home_event.dart';
import 'package:movies/features/main_features/home_feature/presentation/page/home_page.dart';
import 'package:movies/features/main_features/saved_movies_feature/presentation/bloc/saved_movies_bloc.dart';
import 'package:movies/features/main_features/saved_movies_feature/presentation/bloc/saved_movies_event.dart';
import 'package:movies/features/main_features/saved_movies_feature/presentation/pages/saved_movies_page.dart';
import 'package:movies/features/main_features/search_feature/presentation/page/search_page.dart';
import '../../../../../core/data/dependency_injection/injection_container.dart';
import '../../../../../core/data/prefs_helpers/prefs_helper.dart';
import 'search_feature/presentation/bloc/search_bloc.dart';
import 'search_feature/presentation/bloc/search_event.dart';

class MainHolderPage extends StatefulWidget {
  const MainHolderPage({Key? key}) : super(key: key);

  @override
  State<MainHolderPage> createState() => _MainHolderPageState();
}

class _MainHolderPageState extends State<MainHolderPage> {
  int _selectedPage = 0;
  final _homeBloc = getIt<HomeBloc>();
  final _searchBloc = getIt<SearchBloc>();
  final _savedMoviesBloc = getIt<SavedMoviesBloc>();

  List<Widget> _widgetOptions() => <Widget>[
        HomePage(bloc: _homeBloc),
        SearchPage(bloc: _searchBloc),
        SavedMoviesPage(bloc: _savedMoviesBloc),
      ];

  @override
  void initState() {
    super.initState();
    _homeBloc.add(GetGenres());
    _homeBloc.add(GetTrendingMovies());
    _homeBloc.add(GetUpcomingMovies());
    _searchBloc.add(GetMoviesForSearch());
    _savedMoviesBloc.add(GetFavouriteMovies());
    _savedMoviesBloc.add(GetWatchingListMovies());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.primaryBlack,
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(0.0),
        child: AppBar(),
      ),
      bottomNavigationBar: _bottomNavigationBar(),
      body: PageTransitionSwitcher(
        duration: const Duration(milliseconds: 500),
        transitionBuilder: (
          Widget child,
          Animation<double> animation,
          Animation<double> secondaryAnimation,
        ) =>
            FadeThroughTransition(
          fillColor: AppColors.primaryBlack,
          secondaryAnimation: secondaryAnimation,
          animation: animation,
          child: child,
        ),
        child: _widgetOptions()[_selectedPage],
      ),
    );
  }

  Widget _bottomNavigationBar() {
    return SafeArea(
      child: Container(
        height: MediaQuery.of(context).size.height * 0.08,
        decoration: BoxDecoration(
          color: AppColors.primaryBlack,
        ),
        child: Container(
          decoration: BoxDecoration(color: AppColors.primaryBlack),
          child: Padding(
            padding: const EdgeInsets.only(top: 4.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                GestureDetector(
                  onTap: () => _onTabChange(0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(
                        SuperHeroIcons.home,
                        color: _selectedPage == 0
                            ? AppColors.primaryColor
                            : Colors.white,
                        size: 20.sp,
                      ),
                      Text(
                        "Home",
                        style: TextStyle(
                            color: _selectedPage == 0
                                ? AppColors.primaryColor
                                : Colors.white,
                            fontSize: 10.sp),
                      )
                    ],
                  ),
                ),
                GestureDetector(
                  onTap: () => _onTabChange(1),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(
                        SuperHeroIcons.explore,
                        color: _selectedPage == 1
                            ? AppColors.primaryColor
                            : Colors.white,
                        size: 20.sp,
                      ),
                      Text(
                        "Explore",
                        style: TextStyle(
                            color: _selectedPage == 1
                                ? AppColors.primaryColor
                                : Colors.white,
                            fontSize: 10.sp),
                      )
                    ],
                  ),
                ),
                GestureDetector(
                  onTap: () => _onTabChange(2),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.bookmark_border_rounded,
                        color: _selectedPage == 2
                            ? AppColors.primaryColor
                            : Colors.white,
                        size: 22.sp,
                      ),
                      Text(
                        "Saved",
                        style: TextStyle(
                            color: _selectedPage == 2
                                ? AppColors.primaryColor
                                : Colors.white,
                            fontSize: 10.sp),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void _onTabChange(int index) {
    /// we can use bloc to avoid using setState but it's okay...
    setState(() {
      _selectedPage = index;
    });
  }

  List<GButton> tabsWithLogIn = [
    GButton(
        // icon: Icons.settings,
        icon: SuperHeroIcons.home,
        text: 'Home',
        textColor: AppColors.whiteColor,
        iconColor: AppColors.grayColor,
        iconActiveColor: AppColors.whiteColor,
        textStyle: TextStyle(
          fontSize: 12.sp,
          color: AppColors.whiteColor,
        )),
    GButton(
        // icon: Icons.person,
        icon: SuperHeroIcons.explore,
        text: 'Explore',
        textColor: AppColors.whiteColor,
        iconActiveColor: AppColors.whiteColor,
        iconColor: AppColors.grayColor,
        textStyle: TextStyle(fontSize: 12.sp, color: AppColors.whiteColor)),
    GButton(
        // icon: Icons.menu_book_sharp,
        icon: Icons.bookmark_border_outlined,
        text: 'Saved',
        textColor: AppColors.whiteColor,
        iconColor: AppColors.grayColor,
        iconActiveColor: AppColors.whiteColor,
        textStyle: TextStyle(fontSize: 12.sp, color: AppColors.whiteColor)),
  ];
}
