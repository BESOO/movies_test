import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:movies/core/utils/extensions/extension_on_context.dart';
import 'package:movies/core/utils/widgets/custom_loader_widgets.dart';
import 'package:movies/core/utils/widgets/input_text_field.dart';
import 'package:movies/features/main_features/home_feature/presentation/widgets/movie_card_widget.dart';
import 'package:movies/features/main_features/search_feature/presentation/bloc/search_bloc.dart';
import 'package:movies/features/main_features/search_feature/presentation/bloc/search_event.dart';
import 'package:movies/features/main_features/search_feature/presentation/bloc/search_state.dart';

import '../../../../../core/utils/constants/colors.dart';
import '../../../../../core/utils/widgets/gradient_container_widget.dart';

class SearchPage extends StatefulWidget {
  final SearchBloc bloc;

  const SearchPage({Key? key, required this.bloc}) : super(key: key);

  @override
  State<SearchPage> createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  final _movieNameController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return BlocBuilder(
      bloc: widget.bloc,
      builder: (BuildContext context, SearchState state) {
        return Scaffold(
          appBar: PreferredSize(preferredSize: Size.zero, child: AppBar()),
          body: GradientContainerWidget(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const SizedBox(
                  height: 16,
                ),
                getTextFieldWidget(),
                Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8),
                  child: Text(
                    "${state.moviesByQuery.length} results found",
                    style: TextStyle(
                        color: AppColors.secondaryFontColor, fontSize: 10.sp),
                  ),
                ),
                Expanded(
                  child: state.isLoadingMovies
                      ? CustomLoaderWidgets.searchLoaderWidget(context)
                      : GridView.builder(
                          gridDelegate:
                              SliverGridDelegateWithFixedCrossAxisCount(
                                  crossAxisCount: 2,
                                  mainAxisExtent: context.h * 0.34),
                          itemCount: state.moviesByQuery.isEmpty
                              ? state.movies.length
                              : state.moviesByQuery.length,
                          itemBuilder: (BuildContext context, int index) {
                            return Padding(
                              padding:
                                  const EdgeInsets.only(left: 8.0, right: 8),
                              child: MovieCardWidget(
                                  withWidth: false,
                                  withHeight: true,
                                  height: context.h * 0.27,
                                  withFavouriteIcon: true,
                                  movie: state.moviesByQuery.isEmpty
                                      ? state.movies[index]
                                      : state.moviesByQuery[index],
                                  onAddToFavourite: () {
                                    widget.bloc.add(AddMovieToFavourite(
                                        state.moviesByQuery.isEmpty
                                            ? state.movies[index]
                                            : state.moviesByQuery[index]));
                                    setState(() {});
                                  },
                                  onRemoveFromFavourite: () {
                                    widget.bloc.add(RemoveMovieToFavourite(
                                        state.moviesByQuery.isEmpty
                                            ? state.movies[index]
                                            : state.moviesByQuery[index]));
                                    setState(() {});
                                  },
                                  isFavourite: widget
                                      .bloc.moviesRepository.prefsHelper
                                      .getStoredFavouriteListIds()
                                      .contains(state.moviesByQuery.isEmpty
                                          ? state.movies[index].id
                                          : state.moviesByQuery[index].id)),
                            );
                          }),
                )
              ],
            ),
          ),
        );
      },
    );
  }

  Widget getTextFieldWidget() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16.0),
      child: InputTextField(
          onSubmit: (str) {
            widget.bloc.add(GetMoviesByQuery(str));
          },
          hintText: 'Enter the movie name',
          controller: _movieNameController,
          inputType: TextInputType.name),
    );
  }
}
