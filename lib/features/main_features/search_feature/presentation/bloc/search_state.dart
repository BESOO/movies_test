import 'package:built_value/built_value.dart';
import 'package:movies/core/data/models/movie_model.dart';
import 'package:movies/core/error/failures.dart';

part 'search_state.g.dart';

abstract class SearchState implements Built<SearchState, SearchStateBuilder> {
  SearchState._();

  factory SearchState([Function(SearchStateBuilder b) updates]) = _$SearchState;

  bool get isLoadingMovies;
  bool get errorLoadingMovies;
  bool get moviesLoaded;
  Failure? get failure;
  List<MovieModel> get movies;
  List<MovieModel> get moviesByQuery;

  factory SearchState.initial() {
    return SearchState(
      (b) => b
        ..isLoadingMovies = true
        ..errorLoadingMovies = false
        ..moviesLoaded = false
        ..movies = []
        ..moviesByQuery = []
    );
  }
}
