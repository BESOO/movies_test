import 'package:bloc/bloc.dart';
import 'package:injectable/injectable.dart';
import 'package:movies/core/data/repository/repos/movies_repository.dart';
import 'package:movies/features/main_features/search_feature/presentation/bloc/search_event.dart';
import 'package:movies/features/main_features/search_feature/presentation/bloc/search_state.dart';

@Injectable()
class SearchBloc extends Bloc<SearchEvent, SearchState> {
  final MoviesRepository moviesRepository;

  SearchBloc(this.moviesRepository) : super(SearchState.initial());

  @override
  Stream<SearchState> mapEventToState(SearchEvent event) async* {
    if (event is GetMoviesForSearch) {
      yield* mapToGetMovies();
    } else if (event is GetMoviesByQuery) {
      yield* mapToGetMoviesByQuery(event.query);
    } else if (event is AddMovieToFavourite) {
      moviesRepository.prefsHelper.addMovieIdToFavouriteList(event.movie.id);
      moviesRepository.prefsHelper.addMovieToFavouriteList(event.movie);
      yield state.rebuild((p0) => p0);
    } else if (event is RemoveMovieToFavourite) {
      moviesRepository.prefsHelper.removeMovieIdToFavouriteList(event.movie.id);
      moviesRepository.prefsHelper.removeMovieToFavouriteList(event.movie);
      yield state.rebuild((p0) => p0);
    }
  }

  Stream<SearchState> mapToGetMovies() async* {
    yield state.rebuild((p0) => p0
      ..isLoadingMovies = true
      ..errorLoadingMovies = false
      ..moviesLoaded = false);
    final result = await moviesRepository.getMovies(type: "trending");
    yield* result.fold((l) async* {
      yield state.rebuild((p0) => p0
        ..failure = l
        ..isLoadingMovies = false
        ..errorLoadingMovies = true
        ..moviesLoaded = false);
    }, (r) async* {
      yield state.rebuild((p0) => p0
        ..isLoadingMovies = false
        ..errorLoadingMovies = false
        ..moviesLoaded = true
        ..movies = r);
    });
  }

  Stream<SearchState> mapToGetMoviesByQuery(String query) async* {
    yield state.rebuild((p0) => p0
      ..isLoadingMovies = true
      ..errorLoadingMovies = false
      ..moviesLoaded = false);
    final result = await moviesRepository.moviesByQuery(query: query);
    yield* result.fold((l) async* {
      yield state.rebuild((p0) => p0
        ..failure = l
        ..isLoadingMovies = false
        ..errorLoadingMovies = true
        ..moviesLoaded = false);
    }, (r) async* {
      yield state.rebuild((p0) => p0
        ..isLoadingMovies = false
        ..errorLoadingMovies = false
        ..moviesLoaded = true
        ..moviesByQuery = r);
    });
  }
}
