import 'package:movies/core/data/models/movie_model.dart';

abstract class SearchEvent {}

class GetMoviesForSearch extends SearchEvent {}

class GetMoviesByQuery extends SearchEvent {
  final String query;

  GetMoviesByQuery(this.query);
}

class AddMovieToFavourite extends SearchEvent {
  final MovieModel movie;

  AddMovieToFavourite(this.movie);
}

class RemoveMovieToFavourite extends SearchEvent {
  final MovieModel movie;

  RemoveMovieToFavourite(this.movie);
}
