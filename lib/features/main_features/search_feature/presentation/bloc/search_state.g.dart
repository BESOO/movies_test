// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'search_state.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$SearchState extends SearchState {
  @override
  final bool isLoadingMovies;
  @override
  final bool errorLoadingMovies;
  @override
  final bool moviesLoaded;
  @override
  final Failure? failure;
  @override
  final List<MovieModel> movies;
  @override
  final List<MovieModel> moviesByQuery;

  factory _$SearchState([void Function(SearchStateBuilder)? updates]) =>
      (new SearchStateBuilder()..update(updates))._build();

  _$SearchState._(
      {required this.isLoadingMovies,
      required this.errorLoadingMovies,
      required this.moviesLoaded,
      this.failure,
      required this.movies,
      required this.moviesByQuery})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        isLoadingMovies, r'SearchState', 'isLoadingMovies');
    BuiltValueNullFieldError.checkNotNull(
        errorLoadingMovies, r'SearchState', 'errorLoadingMovies');
    BuiltValueNullFieldError.checkNotNull(
        moviesLoaded, r'SearchState', 'moviesLoaded');
    BuiltValueNullFieldError.checkNotNull(movies, r'SearchState', 'movies');
    BuiltValueNullFieldError.checkNotNull(
        moviesByQuery, r'SearchState', 'moviesByQuery');
  }

  @override
  SearchState rebuild(void Function(SearchStateBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  SearchStateBuilder toBuilder() => new SearchStateBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is SearchState &&
        isLoadingMovies == other.isLoadingMovies &&
        errorLoadingMovies == other.errorLoadingMovies &&
        moviesLoaded == other.moviesLoaded &&
        failure == other.failure &&
        movies == other.movies &&
        moviesByQuery == other.moviesByQuery;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, isLoadingMovies.hashCode);
    _$hash = $jc(_$hash, errorLoadingMovies.hashCode);
    _$hash = $jc(_$hash, moviesLoaded.hashCode);
    _$hash = $jc(_$hash, failure.hashCode);
    _$hash = $jc(_$hash, movies.hashCode);
    _$hash = $jc(_$hash, moviesByQuery.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'SearchState')
          ..add('isLoadingMovies', isLoadingMovies)
          ..add('errorLoadingMovies', errorLoadingMovies)
          ..add('moviesLoaded', moviesLoaded)
          ..add('failure', failure)
          ..add('movies', movies)
          ..add('moviesByQuery', moviesByQuery))
        .toString();
  }
}

class SearchStateBuilder implements Builder<SearchState, SearchStateBuilder> {
  _$SearchState? _$v;

  bool? _isLoadingMovies;
  bool? get isLoadingMovies => _$this._isLoadingMovies;
  set isLoadingMovies(bool? isLoadingMovies) =>
      _$this._isLoadingMovies = isLoadingMovies;

  bool? _errorLoadingMovies;
  bool? get errorLoadingMovies => _$this._errorLoadingMovies;
  set errorLoadingMovies(bool? errorLoadingMovies) =>
      _$this._errorLoadingMovies = errorLoadingMovies;

  bool? _moviesLoaded;
  bool? get moviesLoaded => _$this._moviesLoaded;
  set moviesLoaded(bool? moviesLoaded) => _$this._moviesLoaded = moviesLoaded;

  Failure? _failure;
  Failure? get failure => _$this._failure;
  set failure(Failure? failure) => _$this._failure = failure;

  List<MovieModel>? _movies;
  List<MovieModel>? get movies => _$this._movies;
  set movies(List<MovieModel>? movies) => _$this._movies = movies;

  List<MovieModel>? _moviesByQuery;
  List<MovieModel>? get moviesByQuery => _$this._moviesByQuery;
  set moviesByQuery(List<MovieModel>? moviesByQuery) =>
      _$this._moviesByQuery = moviesByQuery;

  SearchStateBuilder();

  SearchStateBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _isLoadingMovies = $v.isLoadingMovies;
      _errorLoadingMovies = $v.errorLoadingMovies;
      _moviesLoaded = $v.moviesLoaded;
      _failure = $v.failure;
      _movies = $v.movies;
      _moviesByQuery = $v.moviesByQuery;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(SearchState other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$SearchState;
  }

  @override
  void update(void Function(SearchStateBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  SearchState build() => _build();

  _$SearchState _build() {
    final _$result = _$v ??
        new _$SearchState._(
            isLoadingMovies: BuiltValueNullFieldError.checkNotNull(
                isLoadingMovies, r'SearchState', 'isLoadingMovies'),
            errorLoadingMovies: BuiltValueNullFieldError.checkNotNull(
                errorLoadingMovies, r'SearchState', 'errorLoadingMovies'),
            moviesLoaded: BuiltValueNullFieldError.checkNotNull(
                moviesLoaded, r'SearchState', 'moviesLoaded'),
            failure: failure,
            movies: BuiltValueNullFieldError.checkNotNull(
                movies, r'SearchState', 'movies'),
            moviesByQuery: BuiltValueNullFieldError.checkNotNull(
                moviesByQuery, r'SearchState', 'moviesByQuery'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
