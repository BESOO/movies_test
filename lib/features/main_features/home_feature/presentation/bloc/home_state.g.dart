// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'home_state.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$HomeState extends HomeState {
  @override
  final bool canRequestMovies;
  @override
  final bool isLoadingGenres;
  @override
  final bool errorLoadingGenres;
  @override
  final bool genresLoaded;
  @override
  final List<GenreModel> genres;
  @override
  final bool isLoadingTrendingMovies;
  @override
  final bool errorLoadingTrendingMovies;
  @override
  final bool trendingMoviesLoaded;
  @override
  final List<MovieModel> trendingMovies;
  @override
  final bool isLoadingUpcomingMovies;
  @override
  final bool errorLoadingUpcomingMovies;
  @override
  final bool upcomingMoviesLoaded;
  @override
  final List<MovieModel> upcomingMovies;
  @override
  final Failure? failure;

  factory _$HomeState([void Function(HomeStateBuilder)? updates]) =>
      (new HomeStateBuilder()..update(updates))._build();

  _$HomeState._(
      {required this.canRequestMovies,
      required this.isLoadingGenres,
      required this.errorLoadingGenres,
      required this.genresLoaded,
      required this.genres,
      required this.isLoadingTrendingMovies,
      required this.errorLoadingTrendingMovies,
      required this.trendingMoviesLoaded,
      required this.trendingMovies,
      required this.isLoadingUpcomingMovies,
      required this.errorLoadingUpcomingMovies,
      required this.upcomingMoviesLoaded,
      required this.upcomingMovies,
      this.failure})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        canRequestMovies, r'HomeState', 'canRequestMovies');
    BuiltValueNullFieldError.checkNotNull(
        isLoadingGenres, r'HomeState', 'isLoadingGenres');
    BuiltValueNullFieldError.checkNotNull(
        errorLoadingGenres, r'HomeState', 'errorLoadingGenres');
    BuiltValueNullFieldError.checkNotNull(
        genresLoaded, r'HomeState', 'genresLoaded');
    BuiltValueNullFieldError.checkNotNull(genres, r'HomeState', 'genres');
    BuiltValueNullFieldError.checkNotNull(
        isLoadingTrendingMovies, r'HomeState', 'isLoadingTrendingMovies');
    BuiltValueNullFieldError.checkNotNull(
        errorLoadingTrendingMovies, r'HomeState', 'errorLoadingTrendingMovies');
    BuiltValueNullFieldError.checkNotNull(
        trendingMoviesLoaded, r'HomeState', 'trendingMoviesLoaded');
    BuiltValueNullFieldError.checkNotNull(
        trendingMovies, r'HomeState', 'trendingMovies');
    BuiltValueNullFieldError.checkNotNull(
        isLoadingUpcomingMovies, r'HomeState', 'isLoadingUpcomingMovies');
    BuiltValueNullFieldError.checkNotNull(
        errorLoadingUpcomingMovies, r'HomeState', 'errorLoadingUpcomingMovies');
    BuiltValueNullFieldError.checkNotNull(
        upcomingMoviesLoaded, r'HomeState', 'upcomingMoviesLoaded');
    BuiltValueNullFieldError.checkNotNull(
        upcomingMovies, r'HomeState', 'upcomingMovies');
  }

  @override
  HomeState rebuild(void Function(HomeStateBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  HomeStateBuilder toBuilder() => new HomeStateBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is HomeState &&
        canRequestMovies == other.canRequestMovies &&
        isLoadingGenres == other.isLoadingGenres &&
        errorLoadingGenres == other.errorLoadingGenres &&
        genresLoaded == other.genresLoaded &&
        genres == other.genres &&
        isLoadingTrendingMovies == other.isLoadingTrendingMovies &&
        errorLoadingTrendingMovies == other.errorLoadingTrendingMovies &&
        trendingMoviesLoaded == other.trendingMoviesLoaded &&
        trendingMovies == other.trendingMovies &&
        isLoadingUpcomingMovies == other.isLoadingUpcomingMovies &&
        errorLoadingUpcomingMovies == other.errorLoadingUpcomingMovies &&
        upcomingMoviesLoaded == other.upcomingMoviesLoaded &&
        upcomingMovies == other.upcomingMovies &&
        failure == other.failure;
  }

  @override
  int get hashCode {
    var _$hash = 0;
    _$hash = $jc(_$hash, canRequestMovies.hashCode);
    _$hash = $jc(_$hash, isLoadingGenres.hashCode);
    _$hash = $jc(_$hash, errorLoadingGenres.hashCode);
    _$hash = $jc(_$hash, genresLoaded.hashCode);
    _$hash = $jc(_$hash, genres.hashCode);
    _$hash = $jc(_$hash, isLoadingTrendingMovies.hashCode);
    _$hash = $jc(_$hash, errorLoadingTrendingMovies.hashCode);
    _$hash = $jc(_$hash, trendingMoviesLoaded.hashCode);
    _$hash = $jc(_$hash, trendingMovies.hashCode);
    _$hash = $jc(_$hash, isLoadingUpcomingMovies.hashCode);
    _$hash = $jc(_$hash, errorLoadingUpcomingMovies.hashCode);
    _$hash = $jc(_$hash, upcomingMoviesLoaded.hashCode);
    _$hash = $jc(_$hash, upcomingMovies.hashCode);
    _$hash = $jc(_$hash, failure.hashCode);
    _$hash = $jf(_$hash);
    return _$hash;
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper(r'HomeState')
          ..add('canRequestMovies', canRequestMovies)
          ..add('isLoadingGenres', isLoadingGenres)
          ..add('errorLoadingGenres', errorLoadingGenres)
          ..add('genresLoaded', genresLoaded)
          ..add('genres', genres)
          ..add('isLoadingTrendingMovies', isLoadingTrendingMovies)
          ..add('errorLoadingTrendingMovies', errorLoadingTrendingMovies)
          ..add('trendingMoviesLoaded', trendingMoviesLoaded)
          ..add('trendingMovies', trendingMovies)
          ..add('isLoadingUpcomingMovies', isLoadingUpcomingMovies)
          ..add('errorLoadingUpcomingMovies', errorLoadingUpcomingMovies)
          ..add('upcomingMoviesLoaded', upcomingMoviesLoaded)
          ..add('upcomingMovies', upcomingMovies)
          ..add('failure', failure))
        .toString();
  }
}

class HomeStateBuilder implements Builder<HomeState, HomeStateBuilder> {
  _$HomeState? _$v;

  bool? _canRequestMovies;
  bool? get canRequestMovies => _$this._canRequestMovies;
  set canRequestMovies(bool? canRequestMovies) =>
      _$this._canRequestMovies = canRequestMovies;

  bool? _isLoadingGenres;
  bool? get isLoadingGenres => _$this._isLoadingGenres;
  set isLoadingGenres(bool? isLoadingGenres) =>
      _$this._isLoadingGenres = isLoadingGenres;

  bool? _errorLoadingGenres;
  bool? get errorLoadingGenres => _$this._errorLoadingGenres;
  set errorLoadingGenres(bool? errorLoadingGenres) =>
      _$this._errorLoadingGenres = errorLoadingGenres;

  bool? _genresLoaded;
  bool? get genresLoaded => _$this._genresLoaded;
  set genresLoaded(bool? genresLoaded) => _$this._genresLoaded = genresLoaded;

  List<GenreModel>? _genres;
  List<GenreModel>? get genres => _$this._genres;
  set genres(List<GenreModel>? genres) => _$this._genres = genres;

  bool? _isLoadingTrendingMovies;
  bool? get isLoadingTrendingMovies => _$this._isLoadingTrendingMovies;
  set isLoadingTrendingMovies(bool? isLoadingTrendingMovies) =>
      _$this._isLoadingTrendingMovies = isLoadingTrendingMovies;

  bool? _errorLoadingTrendingMovies;
  bool? get errorLoadingTrendingMovies => _$this._errorLoadingTrendingMovies;
  set errorLoadingTrendingMovies(bool? errorLoadingTrendingMovies) =>
      _$this._errorLoadingTrendingMovies = errorLoadingTrendingMovies;

  bool? _trendingMoviesLoaded;
  bool? get trendingMoviesLoaded => _$this._trendingMoviesLoaded;
  set trendingMoviesLoaded(bool? trendingMoviesLoaded) =>
      _$this._trendingMoviesLoaded = trendingMoviesLoaded;

  List<MovieModel>? _trendingMovies;
  List<MovieModel>? get trendingMovies => _$this._trendingMovies;
  set trendingMovies(List<MovieModel>? trendingMovies) =>
      _$this._trendingMovies = trendingMovies;

  bool? _isLoadingUpcomingMovies;
  bool? get isLoadingUpcomingMovies => _$this._isLoadingUpcomingMovies;
  set isLoadingUpcomingMovies(bool? isLoadingUpcomingMovies) =>
      _$this._isLoadingUpcomingMovies = isLoadingUpcomingMovies;

  bool? _errorLoadingUpcomingMovies;
  bool? get errorLoadingUpcomingMovies => _$this._errorLoadingUpcomingMovies;
  set errorLoadingUpcomingMovies(bool? errorLoadingUpcomingMovies) =>
      _$this._errorLoadingUpcomingMovies = errorLoadingUpcomingMovies;

  bool? _upcomingMoviesLoaded;
  bool? get upcomingMoviesLoaded => _$this._upcomingMoviesLoaded;
  set upcomingMoviesLoaded(bool? upcomingMoviesLoaded) =>
      _$this._upcomingMoviesLoaded = upcomingMoviesLoaded;

  List<MovieModel>? _upcomingMovies;
  List<MovieModel>? get upcomingMovies => _$this._upcomingMovies;
  set upcomingMovies(List<MovieModel>? upcomingMovies) =>
      _$this._upcomingMovies = upcomingMovies;

  Failure? _failure;
  Failure? get failure => _$this._failure;
  set failure(Failure? failure) => _$this._failure = failure;

  HomeStateBuilder();

  HomeStateBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _canRequestMovies = $v.canRequestMovies;
      _isLoadingGenres = $v.isLoadingGenres;
      _errorLoadingGenres = $v.errorLoadingGenres;
      _genresLoaded = $v.genresLoaded;
      _genres = $v.genres;
      _isLoadingTrendingMovies = $v.isLoadingTrendingMovies;
      _errorLoadingTrendingMovies = $v.errorLoadingTrendingMovies;
      _trendingMoviesLoaded = $v.trendingMoviesLoaded;
      _trendingMovies = $v.trendingMovies;
      _isLoadingUpcomingMovies = $v.isLoadingUpcomingMovies;
      _errorLoadingUpcomingMovies = $v.errorLoadingUpcomingMovies;
      _upcomingMoviesLoaded = $v.upcomingMoviesLoaded;
      _upcomingMovies = $v.upcomingMovies;
      _failure = $v.failure;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(HomeState other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$HomeState;
  }

  @override
  void update(void Function(HomeStateBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  HomeState build() => _build();

  _$HomeState _build() {
    final _$result = _$v ??
        new _$HomeState._(
            canRequestMovies: BuiltValueNullFieldError.checkNotNull(
                canRequestMovies, r'HomeState', 'canRequestMovies'),
            isLoadingGenres: BuiltValueNullFieldError.checkNotNull(
                isLoadingGenres, r'HomeState', 'isLoadingGenres'),
            errorLoadingGenres: BuiltValueNullFieldError.checkNotNull(
                errorLoadingGenres, r'HomeState', 'errorLoadingGenres'),
            genresLoaded: BuiltValueNullFieldError.checkNotNull(
                genresLoaded, r'HomeState', 'genresLoaded'),
            genres: BuiltValueNullFieldError.checkNotNull(
                genres, r'HomeState', 'genres'),
            isLoadingTrendingMovies: BuiltValueNullFieldError.checkNotNull(
                isLoadingTrendingMovies, r'HomeState', 'isLoadingTrendingMovies'),
            errorLoadingTrendingMovies: BuiltValueNullFieldError.checkNotNull(
                errorLoadingTrendingMovies, r'HomeState', 'errorLoadingTrendingMovies'),
            trendingMoviesLoaded: BuiltValueNullFieldError.checkNotNull(
                trendingMoviesLoaded, r'HomeState', 'trendingMoviesLoaded'),
            trendingMovies: BuiltValueNullFieldError.checkNotNull(trendingMovies, r'HomeState', 'trendingMovies'),
            isLoadingUpcomingMovies: BuiltValueNullFieldError.checkNotNull(isLoadingUpcomingMovies, r'HomeState', 'isLoadingUpcomingMovies'),
            errorLoadingUpcomingMovies: BuiltValueNullFieldError.checkNotNull(errorLoadingUpcomingMovies, r'HomeState', 'errorLoadingUpcomingMovies'),
            upcomingMoviesLoaded: BuiltValueNullFieldError.checkNotNull(upcomingMoviesLoaded, r'HomeState', 'upcomingMoviesLoaded'),
            upcomingMovies: BuiltValueNullFieldError.checkNotNull(upcomingMovies, r'HomeState', 'upcomingMovies'),
            failure: failure);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: deprecated_member_use_from_same_package,type=lint
