import 'package:built_value/built_value.dart';
import 'package:movies/core/data/models/movie_model.dart';
import 'package:movies/core/error/failures.dart';

import '../../../../../core/data/models/genre_model.dart';

part 'home_state.g.dart';

abstract class HomeState implements Built<HomeState, HomeStateBuilder> {
  HomeState._();

  factory HomeState([Function(HomeStateBuilder b) updates]) = _$HomeState;

  bool get canRequestMovies;

  bool get isLoadingGenres;
  bool get errorLoadingGenres;
  bool get genresLoaded;
  List<GenreModel> get genres;

  bool get isLoadingTrendingMovies;
  bool get errorLoadingTrendingMovies;
  bool get trendingMoviesLoaded;
  List<MovieModel> get trendingMovies;

  bool get isLoadingUpcomingMovies;
  bool get errorLoadingUpcomingMovies;
  bool get upcomingMoviesLoaded;
  List<MovieModel> get upcomingMovies;

  Failure? get failure;

  factory HomeState.initial() {
    return HomeState((b) => b
      ..canRequestMovies = true
      ..isLoadingTrendingMovies = true
      ..errorLoadingTrendingMovies = false
      ..trendingMoviesLoaded = false
      ..isLoadingUpcomingMovies = true
      ..errorLoadingUpcomingMovies = false
      ..upcomingMoviesLoaded = false
      ..isLoadingGenres = true
      ..errorLoadingGenres = false
      ..genresLoaded = true
      ..genres = []
      ..trendingMovies = []
      ..upcomingMovies = []
    );
  }
}
