import 'package:bloc/bloc.dart';
import 'package:injectable/injectable.dart';
import 'package:movies/core/data/repository/repos/movies_repository.dart';
import 'package:movies/features/main_features/home_feature/presentation/bloc/home_event.dart';
import 'package:movies/features/main_features/home_feature/presentation/bloc/home_state.dart';

@Injectable()
class HomeBloc extends Bloc<HomeEvent, HomeState> {
  final MoviesRepository moviesRepository;

  HomeBloc(this.moviesRepository) : super(HomeState.initial());

  @override
  Stream<HomeState> mapEventToState(HomeEvent event) async* {
    if (event is GetTrendingMovies) {
      yield* mapToGetTrendingMovies();
    } else if (event is GetUpcomingMovies) {
      yield* mapToGetUpcomingMovies();
    } else if (event is GetGenres) {
      yield* mapToGetGenres();
    }
  }

  Stream<HomeState> mapToGetTrendingMovies() async* {
    yield state.rebuild((p0) => p0
      ..isLoadingTrendingMovies = true
      ..errorLoadingTrendingMovies = false
      ..trendingMoviesLoaded = false);
    final result = await moviesRepository.getMovies(type: "trending");
    yield* result.fold((l) async* {
      yield state.rebuild((p0) => p0
        ..failure = l
        ..isLoadingTrendingMovies = false
        ..errorLoadingTrendingMovies = true
        ..trendingMoviesLoaded = false);
    }, (r) async* {
      yield state.rebuild((p0) => p0
        ..isLoadingTrendingMovies = false
        ..errorLoadingTrendingMovies = false
        ..trendingMoviesLoaded = true
        ..trendingMovies = r);
    });
  }

  Stream<HomeState> mapToGetUpcomingMovies() async* {
    yield state.rebuild((p0) => p0
      ..isLoadingUpcomingMovies = true
      ..errorLoadingUpcomingMovies = false
      ..upcomingMoviesLoaded = false);
    final result = await moviesRepository.getMovies(type: "upcoming");
    yield* result.fold((l) async* {
      yield state.rebuild((p0) => p0
        ..failure = l
        ..isLoadingUpcomingMovies = false
        ..errorLoadingUpcomingMovies = true
        ..upcomingMoviesLoaded = false);
    }, (r) async* {
      yield state.rebuild((p0) => p0
        ..isLoadingUpcomingMovies = false
        ..errorLoadingUpcomingMovies = false
        ..upcomingMoviesLoaded = true
        ..upcomingMovies = r);
    });
  }

  Stream<HomeState> mapToGetGenres() async* {
    yield state.rebuild((p0) => p0
      ..isLoadingGenres = true
      ..errorLoadingGenres = false
      ..genresLoaded = false);
    final result = await moviesRepository.getGenres();
    yield* result.fold((l) async* {
      yield state.rebuild((p0) => p0
        ..failure = l
        ..isLoadingGenres = false
        ..errorLoadingGenres = true
        ..genresLoaded = false);
    }, (r) async* {
      print("-----> ggggrr ${r.length}");
      yield state.rebuild((p0) => p0
        ..isLoadingGenres = false
        ..errorLoadingGenres = false
        ..genresLoaded = true
        ..genres = r);
    });
  }
}
