abstract class HomeEvent {}

class GetCategories extends HomeEvent {}

class GetTrendingMovies extends HomeEvent {}

class GetUpcomingMovies extends HomeEvent {}

class GetGenres extends HomeEvent {}
