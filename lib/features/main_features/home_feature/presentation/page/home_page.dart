import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:movies/core/data/dependency_injection/injection_container.dart';
import 'package:movies/core/utils/constants/colors.dart';
import 'package:movies/core/utils/extensions/extension_on_context.dart';
import 'package:movies/features/main_features/home_feature/presentation/bloc/home_bloc.dart';
import 'package:movies/features/main_features/home_feature/presentation/bloc/home_event.dart';
import 'package:movies/features/main_features/home_feature/presentation/bloc/home_state.dart';
import '../../../../../core/utils/widgets/custom_loader_widgets.dart';
import '../../../../../core/utils/widgets/gradient_container_widget.dart';
import '../widgets/genre_widget.dart';
import '../widgets/movie_card_widget.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HomePage extends StatefulWidget {
  final HomeBloc bloc;

  const HomePage({Key? key, required this.bloc}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder(
      bloc: widget.bloc,
      builder: (BuildContext context, HomeState state) {
        return Scaffold(
          appBar: PreferredSize(preferredSize: Size.zero, child: AppBar()),
          body: GradientContainerWidget(
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 16.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "Welcome Reem 👋",
                              style: TextStyle(
                                  color: AppColors.secondaryFontColor),
                            ),
                            const Text(
                              "Bring popcorn, it's a movie time.",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold),
                            )
                          ],
                        ),
                        SizedBox(
                          width: context.w * 0.2,
                          height: context.w * 0.2,
                        )
                      ],
                    ),
                  ),
                  SizedBox(
                    height: context.h * 0.025,
                  ),
                  getGenresWidget(state),
                  SizedBox(
                    height: context.h * 0.02,
                  ),
                  getTrendingMoviesWidget(state),
                  getUpcomingMoviesWidget(state),
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  Widget getGenresWidget(HomeState state) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                "Category",
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 14.sp),
              ),
              Row(
                children: [
                  Text(
                    "See All",
                    style: TextStyle(
                        color: AppColors.primaryColor, fontSize: 12.sp),
                  ),
                  const SizedBox(
                    width: 3,
                  ),
                  Icon(
                    Icons.arrow_forward_ios,
                    size: 13.sp,
                    color: AppColors.primaryColor,
                  )
                ],
              )
            ],
          ),
        ),
        const SizedBox(
          height: 10,
        ),
        state.isLoadingGenres
            ? CustomLoaderWidgets.genreLoadingWidget(context)
            : SizedBox(
                height: context.h * 0.11,
                child: ListView.builder(
                    itemCount: state.genres.length,
                    scrollDirection: Axis.horizontal,
                    itemBuilder: (BuildContext context, int index) {
                      return GenreWidget(
                        genre: state.genres[index],
                        index: index,
                      );
                    }),
              )
      ],
    );
  }

  Widget getTrendingMoviesWidget(HomeState state) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                "Trending Movies",
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 14.sp),
              ),
            ],
          ),
        ),
        const SizedBox(
          height: 10,
        ),
        state.isLoadingTrendingMovies
            ? CustomLoaderWidgets.homeMoviesLoader(context)
            : SizedBox(
                height: context.h * 0.3,
                child: ListView.builder(
                    itemCount: state.trendingMovies.length,
                    scrollDirection: Axis.horizontal,
                    itemBuilder: (BuildContext context, int index) {
                      return MovieCardWidget(
                          withFavouriteIcon: false,
                          withHeight: false,
                          movie: state.trendingMovies[index]);
                    }),
              )
      ],
    );
  }

  Widget getUpcomingMoviesWidget(HomeState state) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                "Upcoming Movies",
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 14.sp),
              ),
            ],
          ),
        ),
        const SizedBox(
          height: 10,
        ),
        state.isLoadingUpcomingMovies
            ? CustomLoaderWidgets.homeMoviesLoader(context)
            : SizedBox(
                height: context.h * 0.3,
                child: ListView.builder(
                    itemCount: state.upcomingMovies.length,
                    scrollDirection: Axis.horizontal,
                    itemBuilder: (BuildContext context, int index) {
                      return MovieCardWidget(
                          withFavouriteIcon: false,
                          withHeight: false,
                          movie: state.upcomingMovies[index]);
                    }),
              )
      ],
    );
  }
}
