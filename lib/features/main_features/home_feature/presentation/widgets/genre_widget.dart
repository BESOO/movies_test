import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:movies/core/data/models/genre_model.dart';
import 'package:movies/core/utils/constants/colors.dart';
import 'package:movies/core/utils/constants/constants.dart';
import 'package:movies/core/utils/extensions/extension_on_context.dart';

class GenreWidget extends StatelessWidget {
  final GenreModel genre;
  final int index;

  const GenreWidget({Key? key, required this.genre, required this.index})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 10.0, right: 6),
      child: Column(
        children: [
          Container(
            width: context.h * 0.075,
            height: context.h * 0.075,
            decoration: BoxDecoration(
              color: AppColors.categoryBackgroundColor,
              borderRadius: const BorderRadius.all(Radius.circular(8)),
            ),
            child: Padding(
              padding: EdgeInsets.all(16.0),
              child: Center(
                  child: Text(
                index >= emojis.length ? emojis[0] : emojis[index],
                style: TextStyle(fontSize: 25.sp),
              )),
            ),
          ),
          const SizedBox(
            height: 4.0,
          ),
          Center(
              child: Text(
            genre.name,
            style: TextStyle(color: AppColors.grayColor),
          ))
        ],
      ),
    );
  }
}
