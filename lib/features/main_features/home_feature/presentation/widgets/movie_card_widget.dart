import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:movies/core/data/models/movie_model.dart';
import 'package:movies/core/utils/constants/colors.dart';
import 'package:movies/core/utils/constants/endpoints.dart';
import 'package:movies/core/utils/extensions/extension_on_context.dart';
import 'package:movies/core/utils/general/app_screens.dart';

class MovieCardWidget extends StatelessWidget {
  final bool withWidth;
  final bool withHeight;
  final double? height;
  final bool withFavouriteIcon;
  final MovieModel movie;
  final Function? onAddToFavourite;
  final Function? onRemoveFromFavourite;
  final bool? isFavourite;

  const MovieCardWidget({
    Key? key,
    this.withWidth = true,
    this.withHeight = true,
    this.height,
    this.isFavourite,
    this.onAddToFavourite,
    this.onRemoveFromFavourite,
    required this.withFavouriteIcon,
    required this.movie,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 8.0, right: 8.0),
      child: GestureDetector(
        onTap: () {
          Navigator.pushNamed(context, AppScreens.movieDetailsPage,
              arguments: {'movie': movie});
        },
        child: Stack(
          children: [
            Column(
              children: [
                Container(
                  width: withWidth ? context.w * 0.27 : double.infinity,
                  height: withHeight ? height : context.h * 0.2,
                  decoration: BoxDecoration(
                      color: movie.posterPath == null
                          ? Colors.grey.withOpacity(0.1)
                          : Colors.transparent,
                      borderRadius:
                          const BorderRadius.all(Radius.circular(20))),
                  child: ClipRRect(
                      borderRadius: const BorderRadius.all(Radius.circular(20)),
                      child: movie.posterPath == null
                          ? Icon(
                              Icons.image_outlined,
                              color: Colors.grey,
                              size: context.w * 0.15,
                            )
                          : CachedNetworkImage(
                              imageUrl:
                                  EndPoints.imagesBaseURL + movie.posterPath!,
                              fit: BoxFit.cover,
                            )),
                ),
                const SizedBox(
                  height: 8,
                ),
                Container(
                  width: withWidth ? context.w * 0.25 : double.infinity,
                  child: Text(
                    movie.originalTitle,
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 10.sp),
                  ),
                ),
                const SizedBox(
                  height: 2,
                ),
                Container(
                  width: withWidth ? context.w * 0.25 : double.infinity,
                  child: Text(
                    "${movie.voteCount} . Action",
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                        color: AppColors.grayColor,
                        fontWeight: FontWeight.bold,
                        fontSize: 10.sp),
                  ),
                ),
              ],
            ),
            withFavouriteIcon
                ? Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Align(
                      alignment: Alignment.topRight,
                      child: GestureDetector(
                        onTap: () {
                          if (onAddToFavourite != null) {
                            if (isFavourite!) {
                              onRemoveFromFavourite!();
                            } else {
                              onAddToFavourite!();
                            }
                          }
                        },
                        child: Icon(
                          isFavourite! ? Icons.favorite : Icons.favorite_border,
                          color: Colors.red,
                          size: context.h * 0.035,
                        ),
                      ),
                    ),
                  )
                : Container()
          ],
        ),
      ),
    );
  }
}
