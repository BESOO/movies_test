import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:movies/core/utils/general/app_screens.dart';
import 'core/utils/constants/colors.dart';

class App extends StatefulWidget {
  const App({Key? key}) : super(key: key);

  @override
  State<App> createState() => _AppState();
}

class _AppState extends State<App> {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
    return ScreenUtilInit(
      designSize: const Size(360, 690),
      minTextAdapt: true,
      splitScreenMode: true,
      builder: (context, child) {
        return MaterialApp(
          theme: ThemeData(
              fontFamily: "Poppins",
              primaryColor: AppColors.primaryColor,
              appBarTheme: AppBarTheme(color: AppColors.secondaryBlack, elevation: 0.0)),
          debugShowCheckedModeBanner: false,
          title: 'Movies App',
          builder: (context, child) {
            return MediaQuery(
              data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
              child: child!,
            );
          },
          initialRoute: AppScreens.mainHolderPage,
          onGenerateRoute: ScreenGenerator.onGenerate,
        );
      },
    );
  }
}
