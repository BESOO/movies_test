import 'package:flutter/material.dart';
import 'app.dart';
import 'core/data/dependency_injection/injection_container.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await setupDi();
  runApp(const App());
}
